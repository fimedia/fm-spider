# 环境说明
- 打开项目后，使用IntellJ打开，根目录下的build.gradle，选择作为项目打开。
- 选择使用wrapper配置的方式。<br>
- 过一段时间,等待依赖和gradle下载，等待依赖下载就会看到项目在IntelJ里展示。

# 如何运行
直接运行类:com.baocao.business.Application

# 接口示例

## 返回所有列表，只是测试用

```
GET http://localhost:8888/user/list
响应：
[
    {
        "id": 1,
        "userId": "b2993850-03de-4373-badc-9537567cc104",
        "accountType": "fb",
        "toiukne": "***"
    }
]
```

## 返回对应的用户信息

```
GET http://localhost:8888/user/register?id=b2993850-03de-4373-badc-9537567cc104
响应
{
    "id": 1,
    "userId": "b2993850-03de-4373-badc-9537567cc104",
    "accountType": "fb",
    "toiukne": "***"
}
```

## 添加一个用户信息
```
POST请求 http://localhost:8080/user/register
请求头：Content-Type:application/json

请求体：body

{
	"header": {"version":"1.0.0","format":"json","encrpty":"RSA"},
    "body":
    {
     	"data": "ew0KICAgICAgICAJInVzZXJJZCI6ICIiLA0KICAgICAgICAJImdhbWUiOiB7DQogICAgICAgIAkJImNoYW5uZWxJZCI6ICIqKiIsDQogICAgICAgIAkJImNwSWQiOiAiKioqIiwNCiAgICAgICAgCQkiZ2FtZUlkIjogIioqKiINCiAgICAgICAgCX0sDQogICAgICAgIAkiY2xpZW50Ijogew0KICAgICAgICAJCSJ2ZSI6ICIyLjEuMSIsDQogICAgICAgIAkJIm9zIjogImFuZHJvaWQiLA0KICAgICAgICAJCSJtb2RlbCI6ICIqKiIsDQogICAgICAgIAkJInVpZCI6ICIqKioiDQogICAgICAgIAl9LA0KICAgICAgICAJImRhdGEiOiB7DQogICAgICAgIAkJImFjY291bnRUeXBlIjogImZhY2Vib29rIiwNCiAgICAgICAgCQkidG9rZW4iOiAiKioqIiwNCiAgICAgICAgCQkibmFtZSI6ICJodWFnZSIsDQogICAgICAgIAkJInNleCI6ICJuYW4iLA0KICAgICAgICAJCSJpY29uIjogImRmZHNmZGZkZiINCiAgICAgICAgCX0NCiAgICAgICAgICANCiAgICAgIH0="
    }
}
响应
{
    "state": {
        "code": 200,
        "msg": "success",
        "exCode": 10000
    },
    "header": {
        "version": "1.0.0",
        "format": "0",
        "encrpty": "0",
        "compress": "0"
    },
    "body": {
        "data": "eyJ1c2VySWQiOiI0YTZlODdmZS1iYjRlLTRmYWYtYTkxYy0yM2Y4ZjljNDUyN2UiLCJyZWdpc3RlclRpbWUiOiIxNTI5MjcwNzcwNTkxIn0="
    }
}
```

# 3种访问数据库
##切换方式

###通过启动参数控制

如果是部署到服务器的话,我们正常打成jar包，启动时通过 --spring.profiles.active=xxx 来控制加载哪个环境的配置，完整命令如下:

java -jar xxx.jar --spring.profiles.active=dev 表示使用开发环境的配置

java -jar xxx.jar --spring.profiles.active=production 表示使用生产环境的配置

java -jar xxx.jar --spring.profiles.active=demo 表示使用demo环境的配置

###通过修改application.yml

表示使用demo环境的配置
```
spring:
  profiles:
    active: demo
```

## 查看数据
###H2相关配置
spring:
  h2:
    console:
      enabled: true
      path: /console
      settings:
        trace: false
        web-allow-others: false

###使用H2

- 访问http://localhost:8888/console
- 填写 jdbc url：jdbc:h2:mem:h2test , 然后选择Connect

可以参考这里：https://my.oschina.net/merryyou/blog/1649943

##使用mysql
现在配置是
```
    url: jdbc:mysql://127.0.0.1:3306/baochao
    username: baochao
    password: 123456
    driverClassName: com.mysql.jdbc.Driver
```
baochao schema, baochao username, 123456 password

对应的构建脚本在
/main/resources/h2/*.sql
/main/resources/msyql/*.sql


##爬虫运营
##系统位置：
http://147.139.133.188/index

## Note Id,User ID获取
user id:5b7698a286002d000144e730 
https://www.xiaohongshu.com/user/profile/<user_id>?xxxx

举例：https://www.xiaohongshu.com/user/profile/5b7698a286002d000144e730

note id: 5cb85676000000000f02ed43 
https://www.xiaohongshu.com/discovery/item/<note_id>?xxx

举例：https://www.xiaohongshu.com/discovery/item/5cb85676000000000f02ed43

## 触发爬取（选择Crwal User/Note)

### 爬取URL
选取类型Fetch Type, URL

爬取URL输入框中，输入URL，根据note、user自动识别

点击立即提交

### 爬取用户
选取类型Fetch Type, User

爬取用户在UserId框里输入User Id

点击立即提交

### 爬取单篇
选取类型Fetch Type, Note

爬取单篇在NoteId框里输入Note Id

点击立即提交

## 下载内容(Download)

###下载用户
在UserId框里输入User Id

点击立即提交

###下载单篇
在NoteId框里输入Note Id

点击立即提交


#爬虫服务器信息

##服务器：
i-k1a5fq4u7fpjq71n109q
远程连接密码  763016
实例密码： A!s2d3f4
公网： 147.139.133.188
内网： 172.31.186.158
数据库：
域名：rm-k1a1z9r03ys2l7h1n.mysql.ap-southeast-5.rds.aliyuncs.com
端口： 3306
用户密码：  fm_fcs_indonesia  A!s2d3f4g5
库名： fm_stat_id

##服务部署规范
1.用户: fm
2.中间件部署目录： /home/fm/local/{中间件目录}
3.应用部署目录  /home/fm/local/app
4.日志打点目录 /home/fm/local/app/logs

##数据库
149.129.233.193(公)
172.31.143.191(私有)
rm-k1ak3zg5n5auep246.mysql.ap-southeast-5.rds.aliyuncs.com:3306
 fm_fcs_indonesia  用户名

#维护脚本
