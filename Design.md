# 整体架构

## 概述

项目使用的是spring boot+jpa+h2、mysql的方式提供restful API。

后续可以使用spring mvc和前端框架搭建后台。
如果有日志分析需求，可以使用写日志和ES等做一些全文检索相关的内容。

## 层次
## controller
这里类似与Facade，一层门面，对于http接口，或者网页处理，都放在这一层

## serivce
业务逻辑封装，主要用于譬如jpa调用、对外接口调用封装等。业务逻辑实现层

## model
持久化层，持久化对象的定义和持久化服务JPA的提供

## protocl
协议制定的部分，用于controller

# 项目结构
分为两部分：
- server_infrastructure：服务器基建部分，包括：编码，加解密等部分，后续可以添加异常处理，序列化等基础能力。
主要用于以后出现多个服务器后台的时候，这一公用部分可以扩展。

- runtime_server：restful API后台，主要提供http的接口封装（controller）、业务逻辑（Service）、持久化（model）。
主要的三层结构。

## 重要文件说明
### 程序main入口 com.baocao.business.Application.java
用于程序启动。目前设置@SpringBootApplication，因此搜索所有子包下的类（com.baocao.business）

### 配置文件:resources/applcation.yml
程序配置，包括端口号、数据库配置，都在这里，目前有三种模式：
详见 README.md 的 “# 3种访问数据库”
举例：
```
  datasource:
    url: jdbc:h2:~/testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE #数据库connection str,这里表示存储在用户目录下的testdb数据库文件
    platform: h2
    username: sa
    password:
    driverClassName: org.h2.Driver #连接数据库驱动程序

  jpa:
    database-platform: org.hibernate.dialect.H2Dialect #hiberate方言，有h2/mysql的支持
    hibernate:
      ddl-auto: update
    properties:
      hibernate:
        show_sql: true
        use_sql_comments: true
        format_sql: true
```
可以看到目前配置是会输出执行的SQL，包括数据库创建和查询。

### sql脚本：/resources/下
h2\mysql都支持，因为现在默认是会在数据库类创建，

# 开发示例
## 各部分参考及要点

### controller层
类上需要有注解：
- @RestController ：帮助程序找到这个类会被spring扫描托管
- @RequestMapping(value = "/order")：制定这个restful接口的路径，这里是：order

方法上：
- @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/list")
- 标示使用get方法，然后映射到list（结合controller，整体是/order/list)，然后要求meta type，header里说是application/json类型。

成员上：
- 需要使用@Autowired，找到对应的托管对象

### Service
- 类上需要有注解：@Service
- 被其他地方引用时，需要使用@Autowired

### model
- 一般会有Entity对象，类声明有：@Entity。
- 有主键需要声明id，和自增，主要用于数据库主键 @Id @GeneratedValue(strategy = GenerationType.AUTO)

使用Repository作为持久化实现。
- 注解需要@Repository，方便spring发现。
- 类需要，继承 JpaRepository<DataObject, ID Type>
 

# 构建和部署
## 构建一个可运行包
使用gradlew bootJar命令，可以获得在，runtime_server/build/libs/runtime_server-0.1.0.jar 的一个可执行包。

这个包可以通过:
java -jar runtime_server/build/libs/runtime_server-0.1.0.jar 直接运行

如果需要制定不同的运行配置，可以使用不同命令控制

java -jar runtime_server-0.1.0.jar --spring.profiles.active=dev 表示使用开发环境的配置

java -jar runtime_server-0.1.0.jar --spring.profiles.active=production 表示使用生产环境的配置

java -jar runtime_server-0.1.0.jar --spring.profiles.active=demo 表示使用demo环境的配置

## 数据库连接
详见 README.md 的 “# 3种访问数据库”

### mysql的安装
可以使用绿色版，例如，mysql-5.7.17-winx64，解压就可以参考：https://blog.csdn.net/zbq2018/article/details/79335166

使用MySQLWorkbench去连接数据库。可以创建用户baochao, 设置密码为123456，建立默认数据库schema：baochao。
因此默认的连接字符串在application.yml中的值是：    
```
url: jdbc:mysql://127.0.0.1:3306/baochao
username: baochao
password: 123456
```

# 其他教程
spring boot官网
https://spring.io/projects/spring-boot

Spring-Boot使用profile来配置不同环境的配置文件
https://my.oschina.net/u/2278977/blog/807958

spring boot+jpa+mysql举例
https://blog.csdn.net/a499477783/article/details/78648110