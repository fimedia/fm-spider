scp ~/work/xhsspider/codes/SpiderServer/runtime_server/build/libs/runtime_server-0.1.0.jar fm@147.139.133.188:/home/fm/local/app/spider/

# nohup java -jar runtime_server-0.1.0.jar > test.out 2>&1 &

nohup /home/fm/local/jdk/bin/java -jar -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC -XX:+PrintGCDetails -Xloggc:./logs/gclogs ./runtime_server-0.1.0.jar > logs/application.out 2>&1 &