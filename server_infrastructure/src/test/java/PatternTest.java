import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baocao.server.base.util.JsonUtil;
import com.baocao.server.base.util.RegexUtil;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternTest {

    @Test
    public void testBackgroundImage() {
        String pic = "background-image: url(\"//ci.xiaohongshu.com/8560feb7-6e4d-3a2b-80b5-b576d9314cba?imageView2/2/w/1080/format/jpg\");";
        String http = pic.replace("background-image: url(\"", "http:").replace("\");", "");

        String expected = "http://ci.xiaohongshu.com/8560feb7-6e4d-3a2b-80b5-b576d9314cba?imageView2/2/w/1080/format/jpg";

        System.out.println(http);
        Assert.assertEquals(expected, http);
    }
    @Test
    public void testGetPathFromUrl(){
        String URL_REVIEW_DETAIL = ".*/api/.*";
        Pattern pattern = Pattern.compile(URL_REVIEW_DETAIL);

        String input = "https://reviews.femaledaily.com/api/treatment-color?h=1";
        Matcher matcher = pattern.matcher(input);
        System.out.println(matcher.group(0));
    }

    @Test
    public void testFemaleDailyDetail() {
        String URL_REVIEW_DETAIL = "https://reviews\\.femaledaily\\.com/products/[^/]+/[^/]+/.+";
        Pattern reviewUrlPattern = Pattern.compile(URL_REVIEW_DETAIL);

        String input = "https://reviews.femaledaily.com/products/treatment-color/scalp-treatment/xl-professionnel/xl-professionel-anti-dandruff-hair-scrub?tab=reviews&cat=&cat_id=0&age_range=&skin_type=&skin_tone=&skin_undertone=&hair_texture=&hair_type=&order=newest&page=1";
        Matcher matcher = reviewUrlPattern.matcher(input);
        Assert.assertTrue(matcher.matches());
    }

    @Ignore
    @Test
    public void testFemaleDailyPagingPatternNotMatchDetailUrl() {

        //https://reviews.femaledaily.com/shampoo-conditioner/conditioner-20?brand=&order=popular&page=27
        String URL_PAGING = "https://reviews\\.femaledaily\\.com/.+&page=\\d+";
        Pattern reviewUrlPattern = Pattern.compile(URL_PAGING);

        String input = "https://reviews.femaledaily.com/products/treatment-color/scalp-treatment/xl-professionnel/xl-professionel-anti-dandruff-hair-scrub?tab=reviews&cat=&cat_id=0&age_range=&skin_type=&skin_tone=&skin_undertone=&hair_texture=&hair_type=&order=newest&page=1";
        Matcher matcher = reviewUrlPattern.matcher(input);
        Assert.assertFalse(matcher.matches());
    }

    @Test
    public void testFemaleDailyPaging() {

        //https://reviews.femaledaily.com/shampoo-conditioner/conditioner-20?brand=&order=popular&page=27
        String URL_PAGING = "https://reviews\\.femaledaily\\.com/.+&page=\\d+";
        Pattern reviewUrlPattern = Pattern.compile(URL_PAGING);

        String input = "https://reviews.femaledaily.com/shampoo-conditioner/conditioner-20?brand=&order=popular&page=27";

        //https://reviews.femaledaily.com/lips/lip-palette?brand=&order=popular&page=1
        //https://reviews.femaledaily.com/products/lips/lip-palette?brand=&order=popular&page=1
//        input = "https://reviews.femaledaily.com/products/cheek/contour?brand=&order=popular&page=1";
        Matcher matcher = reviewUrlPattern.matcher(input);
        Assert.assertTrue(matcher.matches());
    }

    @Test
    public void testPinerest(){
        Pattern mPattern = Pattern.compile("\"url\": \"([^\"]+(?<=\\.mp4))");

        String content = "{\"url\": \"https://i.pinimg.com/170x/03/17/92/0317921e540fc28ebcf449eb030d081d.jpg\"   {\"url\": \"https://i.pinimg.com/170x/03/17/92/0317921e540fc28ebcf449eb030d081d.mp4\"";
        String fileName = RegexUtil.extract(mPattern, content);

        Assert.assertEquals("https://i.pinimg.com/170x/03/17/92/0317921e540fc28ebcf449eb030d081d.mp4", fileName);
    }

    @Test
    public void getPicName() {
        Pattern mPicUrlPattern = Pattern.compile("http://ci.xiaohongshu.com/([^?]+)");
        String picUrl = "http://ci.xiaohongshu.com/e0fe9b37-5687-5254-bfe2-d35e208b68b0?imageView2/2/w/1080/format/jpg";

        String fileName = RegexUtil.extract(mPicUrlPattern, picUrl);

        Assert.assertEquals("e0fe9b37-5687-5254-bfe2-d35e208b68b0", fileName);
    }

    @Test
    public void getNoteId() {
        Pattern mPattern = Pattern.compile("https://www.xiaohongshu.com/discovery/item/([^?]+)");
        String target = "https://www.xiaohongshu.com/discovery/item/5cb85676000000000f02ed43?xhsshare=CopyLink&appuid=56b98b59b8ce1a19a6f23943&apptime=1558315388";

        String result = RegexUtil.extract(mPattern, target);
        Assert.assertEquals("5cb85676000000000f02ed43", result);

        target = "https://www.xiaohongshu.com/discovery/item/5cb85676000000000f02ed43";
        result = RegexUtil.extract(mPattern, target);
        Assert.assertEquals("5cb85676000000000f02ed43", result);
    }


    @Test
    public void getVideo() {
        String content = "{\"video\":{\"url\":\"http:\\u002F\\u002Fv.xiaohongshu.com\\u002F2e7b21bd3816b88f63ff8259818636abefa02a7c?sign=4f62d1c646a8e27af0c208fa6cce3d83&t=5d051600\",\"pic\":\"\\u002F\\u002Fci.xiaohongshu.com\\u002F963428ea-b4d3-373f-a5b4-6a9ba33db20d\"}";

        Pattern mPattern = Pattern.compile("\\{\"video\":(\\{[^}]+\\})");

        String result = RegexUtil.extract(mPattern, content);
        System.out.println(result);
        JSONObject object = JSON.parseObject(result);
        String url = object.getString("url");
        String pic = object.getString("pic");

        System.out.println(url);
        System.out.println(pic);
    }

    @Test
    public void test() {
        String content = "<a class=\"jsx-1620353837 breadcrumb-text\" href=\"/eye-treatment/eye-serum?brand=&amp;order=popular&amp;page=1\">Eye Serum</a>";

        Pattern categoryPattern = Pattern.compile(">([^<]+)</a>");
        String result = RegexUtil.extract(categoryPattern, content);

        System.out.println(result);
    }

    @Test
    public void testPrice() {

        Pattern pricePattern = Pattern.compile("(USD\\s*[\\d\\.]+|Rp\\.\\s*[\\d\\.]+)");

        String[] price1 = new String[]{
                "<div class=\"jsx-1245854910 pd-price\">USD 68<p class=\"jsx-1245854910\"></p></div>",
                "<div class=\"jsx-1245854910 pd-price\">Rp.  250.000<p class=\"jsx-1245854910\"></p></div>"
        };

        for (String p:price1) {
            String value = RegexUtil.extract(pricePattern, p);
            System.out.println(value);
        }
    }

    @Test
    public void pinterestaTest() {

    }
            //https://id.pinterest.com/pin/304767099783874047/
}
