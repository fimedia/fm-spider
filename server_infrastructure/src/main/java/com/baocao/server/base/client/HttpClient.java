package com.baocao.server.base.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import com.baocao.server.base.converter.MyGsonHttpMessageConverter;
import com.baocao.server.base.exception.ErrorDesc;
import com.baocao.server.base.util.JsonUtil;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class HttpClient
{
    private Logger mLogger = LoggerFactory.getLogger(this.getClass());

    private String mConfigureServerUrl;

    @Autowired
    private Environment env;

    private RestTemplate mRestTemplate;

    @SuppressWarnings("rawtypes")
    public HttpClient()
    {
        org.apache.http.client.HttpClient httpClient = HttpClientBuilder.create().build();
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
        mRestTemplate = new RestTemplate(requestFactory);

        GsonHttpMessageConverter converter = new MyGsonHttpMessageConverter();
        converter.setGson(JsonUtil.getGson());

        // ). use custom JSON converter first
        HttpMessageConverter jacksonConverter = null;
        for (HttpMessageConverter msgConverter : mRestTemplate.getMessageConverters())
        {
            if (msgConverter instanceof MappingJackson2HttpMessageConverter)
            {
                jacksonConverter = msgConverter;
            }
        }
        int index = mRestTemplate.getMessageConverters().indexOf(jacksonConverter);

        if (index < 0)
        {
            index = 0;
        }
        // ). use custom JSON converter first.
        mRestTemplate.getMessageConverters().add(index, converter);

        mRestTemplate.setErrorHandler(new ResponseErrorHandler()
        {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException
            {
                HttpStatus statusCode = response.getStatusCode();
                return statusCode != HttpStatus.OK && statusCode != HttpStatus.NO_CONTENT;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException
            {

                String json = convertStreamToString(response.getBody());
                mLogger.info("error response data : {}", json);

                ErrorDesc errorDesc = JsonUtil.toObject(json, ErrorDesc.class);

                throw new ClientErrorCodeException(response.getStatusCode().value(), errorDesc);
            }
        });
    }

    /**
     * Convert input stream into string<br>
     * Reference:<br>
     * 
     * <pre>
     * http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string
     * </pre>
     * 
     * @param is
     *            input stream
     * @return string value from stream
     */
    static String convertStreamToString(java.io.InputStream is)
    {
        //
        try (java.util.Scanner scanner = new java.util.Scanner(is, "UTF-8"))
        {
            return scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
        }
    }

    public RestTemplate getRestTemplate()
    {
        return mRestTemplate;
    }

    public <T> HttpEntity<T> buildJsonRequest(T body)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set(HttpHeaders.ACCEPT_ENCODING, MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<T> entity = new HttpEntity<T>(body, headers);
        return entity;
    }

    public <T> HttpEntity<byte[]> buildJsonGzipRequest(T body)
    {
        String json = JsonUtil.toString(body);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream gzos = null;
        try
        {
            gzos = new GZIPOutputStream(baos);
            gzos.write(json.getBytes("UTF-8"));
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally
        {
            if (gzos != null)
                try
                {
                    gzos.close();
                }
                catch (IOException ignore)
                {
                }
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(HttpHeaders.CONTENT_ENCODING, "gzip");

        HttpEntity<byte[]> entity = new HttpEntity<byte[]>(baos.toByteArray(), headers);
        return entity;
    }

    public <T> HttpEntity<T> addGzipHeader(HttpEntity<T> httpEntity)
    {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate");

        HttpHeaders originalHeaders = httpEntity.getHeaders();
        for (String key : originalHeaders.keySet())
        {
            List<String> values = originalHeaders.get(key);
            for (String value : values)
            {
                headers.add(key, value);
            }
        }
        T body = httpEntity.getBody();
        return new HttpEntity<T>(body, headers);
    }

    public <T> ResponseEntity<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType, Object... uriVariables) throws RestClientException
    {
        return mRestTemplate.exchange(getServiceUrl() + url, method, requestEntity, responseType, uriVariables);
    }

    public String getServiceUrl()
    {
        if (mConfigureServerUrl != null)
        {
            return mConfigureServerUrl;
        }
        return env.getProperty("client.server.url");
    }

    public void setConfigureServerUrl(String configureServerUrl)
    {
        this.mConfigureServerUrl = configureServerUrl;
    }

}
