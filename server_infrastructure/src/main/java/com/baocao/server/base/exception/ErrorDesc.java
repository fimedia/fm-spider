package com.baocao.server.base.exception;

import com.google.gson.annotations.SerializedName;

//FIXME: move to server protocol to support error msg
public class ErrorDesc
{
    @SerializedName("error")
    private int mErrCode;

    @SerializedName("msg")
    private String mErrMsg;

    /**
     * @param errCode
     * @param errMsg
     */
    public ErrorDesc(int errCode, String errMsg)
    {
        this.mErrCode = errCode;
        this.mErrMsg = errMsg;
    }

    public int getErrCode()
    {
        return mErrCode;
    }

    public String getErrMsg()
    {
        return mErrMsg;
    }
}
