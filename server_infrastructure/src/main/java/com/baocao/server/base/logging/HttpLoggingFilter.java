package com.baocao.server.base.logging;

import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baocao.server.base.compress.RequestDecompressFilter;
import com.baocao.server.base.util.OrderFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpLoggingFilter implements OrderFilter
{
    /**
     * It should be when the filter can use plain text request and response data
     * 
     * @see RequestDecompressFilter
     */
    private static int ORDER = 2;

    private Logger mLogger = LoggerFactory.getLogger("HttpLogging");


    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        if (mLogger.isDebugEnabled())
        {
            BufferedRequestWrapper requestWrapper = new BufferedRequestWrapper(httpServletRequest);
            mLogger.info("receive request:");

            Enumeration<String> headerNames = requestWrapper.getHeaderNames();
            logHeaders(requestWrapper, headerNames);
            mLogger.info(requestWrapper.getRequestBody());

            httpServletRequest = requestWrapper;
            httpServletResponse = new BufferedResponseWrapper(httpServletResponse);
        }

        chain.doFilter(httpServletRequest, httpServletResponse);

        if (mLogger.isDebugEnabled())
        {
            BufferedResponseWrapper responseWrapper = (BufferedResponseWrapper) httpServletResponse;
            mLogger.info("write response:");
            Collection<String> headerNames = responseWrapper.getHeaderNames();
            logHeaders(responseWrapper, headerNames);
            mLogger.info(responseWrapper.getResponseBody());
        }
    }

    private void logHeaders(BufferedRequestWrapper requestWrapper, Enumeration<String> headerNames)
    {
        while (headerNames.hasMoreElements())
        {
            String header = headerNames.nextElement();
            String value = requestWrapper.getHeader(header);
            mLogger.info("header:" + header + "=" + value);
        }
    }

    private void logHeaders(BufferedResponseWrapper responseWrapper, Collection<String> headerNames)
    {
        Iterator<String> it = headerNames.iterator();
        while (it.hasNext())
        {
            String header = it.next();
            String value = responseWrapper.getHeader(header);
            mLogger.info("header:" + header + "=" + value);
        }
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public int getOrder()
    {
        return ORDER;
    }
}
