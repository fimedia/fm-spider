//package com.baocao.server.base.config;
//
//import java.util.List;
//
//import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.web.config.EnableSpringDataWebSupport;
//import org.springframework.http.converter.HttpMessageConverter;
//import org.springframework.http.converter.json.GsonHttpMessageConverter;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//
//import com.baocao.server.base.converter.MyGsonHttpMessageConverter;
//import com.baocao.server.base.util.JsonUtil;
//
//@Configuration
//@EnableWebMvc
//@EnableSpringDataWebSupport
//public class WebConfig extends WebMvcAutoConfigurationAdapter
//{
//
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters)
//    {
//        GsonHttpMessageConverter converter = new MyGsonHttpMessageConverter();
//        converter.setGson(JsonUtil.getGson());
//        converters.add(converter);
//        super.configureMessageConverters(converters);
//    }
//}
