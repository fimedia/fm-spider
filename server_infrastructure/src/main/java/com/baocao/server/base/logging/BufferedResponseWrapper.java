package com.baocao.server.base.logging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class BufferedResponseWrapper extends HttpServletResponseWrapper
{
    private TeeServletOutputStream mTeeOutputStream;

    private static class TeeOutputStream extends OutputStream
    {
        private OutputStream mChainStream;
        private OutputStream mTeeStream;

        public TeeOutputStream(OutputStream chainStream, OutputStream teeStream)
        {
            mChainStream = chainStream;
            mTeeStream = teeStream;
        }

        @Override
        public synchronized void write(int b) throws IOException
        {
            mChainStream.write(b);
            mTeeStream.write(b);
        }

        @Override
        public void close() throws IOException
        {
            mChainStream.close();
            mTeeStream.close();
        }

        @Override
        public void flush() throws IOException
        {
            mChainStream.flush();
            mTeeStream.flush();
        }
    }

    public class TeeServletOutputStream extends ServletOutputStream
    {
        private final TeeOutputStream targetStream;

        public TeeServletOutputStream(OutputStream one, OutputStream two)
        {
            targetStream = new TeeOutputStream(one, two);
        }

        @Override
        public void write(int b) throws IOException
        {
            this.targetStream.write(b);
        }

        @Override
        public void flush() throws IOException
        {
            super.flush();
            this.targetStream.flush();
        }

        @Override
        public void close() throws IOException
        {
            super.close();
            this.targetStream.close();
        }

        @Override
        public boolean isReady()
        {
            return false;
        }

        @Override
        public void setWriteListener(WriteListener writeListener)
        {

        }
    }

    private ByteArrayOutputStream mByteArrayOutputStream;

    public BufferedResponseWrapper(HttpServletResponse response) throws IOException
    {
        super(response);
        mByteArrayOutputStream = new ByteArrayOutputStream();
        mTeeOutputStream = new TeeServletOutputStream(super.getResponse().getOutputStream(), mByteArrayOutputStream);
    }

    @Override
    public PrintWriter getWriter() throws IOException
    {
        return super.getResponse().getWriter();
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException
    {
        return mTeeOutputStream;
    }

    public String getResponseBody()
    {
        return mByteArrayOutputStream.toString();
    }

}
