package com.baocao.server.base.compress;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.baocao.server.base.util.OrderFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.baocao.server.base.logging.HttpLoggingFilter;

public class RequestDecompressFilter implements OrderFilter
{
    /**
     * It should be before HttpLoggingFilter
     * 
     * @see HttpLoggingFilter
     */
    private static int ORDER = 1;

    private Logger mLogger = LoggerFactory.getLogger(this.getClass());

    @Override
    public int getOrder()
    {
        return ORDER;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        mLogger.info("init filter:{}", this.getClass().getCanonicalName());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        String contentEncoding = httpServletRequest.getHeader("Content-Encoding");
        if (contentEncoding != null && contentEncoding.indexOf("gzip") > -1)
        {
            try
            {
                final InputStream decompressStream = StreamHelper.decompressStream(httpServletRequest.getInputStream());

                httpServletRequest = new HttpServletRequestWrapper(httpServletRequest)
                {

                    @Override
                    public ServletInputStream getInputStream() throws IOException
                    {
                        return new DecompressServletInputStream(decompressStream);
                    }

                    @Override
                    public BufferedReader getReader() throws IOException
                    {
                        return new BufferedReader(new InputStreamReader(decompressStream));
                    }
                };
            }
            catch (IOException e)
            {
                mLogger.error("error while handling the request", e);
            }
        }

        chain.doFilter(httpServletRequest, response);
    }

    @Override
    public void destroy()
    {
        mLogger.info("destroy filter:{}", this.getClass().getCanonicalName());
    }

    public static class DecompressServletInputStream extends ServletInputStream
    {
        private InputStream inputStream;

        public DecompressServletInputStream(InputStream input)
        {
            inputStream = input;

        }

        @Override
        public boolean isFinished()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isReady()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void setReadListener(ReadListener readListener)
        {
            // TODO Auto-generated method stub
        }

        @Override
        public int read() throws IOException
        {
            return inputStream.read();
        }

        @Override
        public int available() throws IOException
        {
            return inputStream.available();
        }

        @Override
        public int read(byte[] buf, int off, int len) throws IOException
        {
            return inputStream.read(buf, off, len);
        }
    }
}
