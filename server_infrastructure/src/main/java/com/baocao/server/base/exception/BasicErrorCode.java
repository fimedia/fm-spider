package com.baocao.server.base.exception;

public enum BasicErrorCode
{
    E_BAD_REQUEST(400, "invalid request"), E_UNAUTHORIZED(401, "unauthorized."), E_NOT_FOUND(404, "resource not found"), E_INTERNAL_SERVER_ERROR(500, "internal server error");

    private int httpStatusCode;
    private int code;
    private String desc;

    /**
     * Basic Error code with http status code ,error code, desc
     * 
     * @param httpStatusCode
     * @param code
     * @param desc
     */
    private BasicErrorCode(int httpStatusCode, int code, String desc)
    {
        this.httpStatusCode = httpStatusCode;
        this.code = code;
        this.desc = desc;
    }

    /**
     * http status code and code are the same.
     * 
     * @param code
     * @param desc
     */
    private BasicErrorCode(int code, String desc)
    {
        this.httpStatusCode = code;
        this.code = code;
        this.desc = desc;
    }

    public String getDesc()
    {
        return this.desc;
    }

    public int getHttpStatusCode()
    {
        return httpStatusCode;
    }

    public int getCode()
    {
        return code;
    }
}
