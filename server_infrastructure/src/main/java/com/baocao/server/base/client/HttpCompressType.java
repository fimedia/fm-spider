package com.baocao.server.base.client;

public enum HttpCompressType
{
    REQUEST_COMPRESS, REQUEST_UNCOMPRESS, RESPONSE_COMPRESS, RESPONSE_UNCOMPRESS,
}
