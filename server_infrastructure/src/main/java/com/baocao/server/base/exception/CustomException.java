package com.baocao.server.base.exception;

import javax.servlet.http.HttpServletResponse;

public class CustomException extends RuntimeException
{
    private static final long serialVersionUID = 5339944435424675276L;

    private int mHttpStatusCode;
    private int mErrCode;
    private String mErrMsg;

    public CustomException(BasicErrorCode basicError)
    {
        this(basicError.getHttpStatusCode(), basicError.getCode(), basicError.getDesc());
    }

    public CustomException(int errCode)
    {
        this(HttpServletResponse.SC_BAD_REQUEST, errCode, null);
    }

    public CustomException(int errCode, String errMsg)
    {
        this(HttpServletResponse.SC_BAD_REQUEST, errCode, errMsg);
    }

    public CustomException(int httpStatusCode, int errCode, String errMsg)
    {
        this(httpStatusCode, errCode, errMsg, null);
    }

    public CustomException(int httpStatusCode, int errCode, String errMsg, Throwable cause)
    {
        super(cause);
        this.mHttpStatusCode = httpStatusCode;
        this.mErrCode = errCode;
        this.mErrMsg = errMsg;
    }

    public int getErrCode()
    {
        return mErrCode;
    }

    public String getErrMsg()
    {
        return mErrMsg;
    }

    public int getHttpStatusCode()
    {
        return mHttpStatusCode;
    }
}
