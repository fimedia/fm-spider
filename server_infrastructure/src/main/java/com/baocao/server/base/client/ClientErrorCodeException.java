package com.baocao.server.base.client;

import com.baocao.server.base.exception.ErrorDesc;

public class ClientErrorCodeException extends RuntimeException
{

    /**
     * 
     */
    private static final long serialVersionUID = 3161040180921006213L;

    private int mHttpStatusCode;
    private int mErrCode;
    private String mErrMsg;

    public ClientErrorCodeException(int httpStatusCode, ErrorDesc errorDesc)
    {
        super();
        mHttpStatusCode = httpStatusCode;
        mErrCode = errorDesc.getErrCode();
        mErrMsg = errorDesc.getErrMsg();
    }

    public ClientErrorCodeException(int httpStatusCode, int errCode, String errMsg)
    {
        super();
        mHttpStatusCode = httpStatusCode;
        mErrCode = errCode;
        mErrMsg = errMsg;
    }

    public int getHttpStatusCode()
    {
        return mHttpStatusCode;
    }

    public int getErrCode()
    {
        return mErrCode;
    }

    public String getErrMsg()
    {
        return mErrMsg;
    }

}
