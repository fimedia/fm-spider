package com.baocao.server.base.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {

    public static String extract(Pattern pattern, String target) {
        String result = "";
        Matcher matcher = pattern.matcher(target);
        if(matcher.find()) {
            if (matcher.groupCount() >= 1) {
                result = matcher.group(1);
            }
        }
        return result;
    }

}
