package com.baocao.server.base.converter;

import java.lang.reflect.Type;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

/**
 * Temporary solution for gson and jackson mixture usage, replace with gson
 * converter
 * 
 * @see GsonHttpMessageConverter
 * @author chenle
 *
 */
public class MyGsonHttpMessageConverter extends GsonHttpMessageConverter
{
    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType)
    {
        return canRead(mediaType) && supports(clazz);
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType)
    {
        return canRead(mediaType) && supports(type.getClass());
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType)
    {
        return canWrite(mediaType) && supports(clazz);
    }

    @Override
    protected boolean supports(Class<?> clazz)
    {
        boolean isHateoasEntity = clazz.getName().startsWith("org.springframework.hateoas");
        return !isHateoasEntity;
    }
}