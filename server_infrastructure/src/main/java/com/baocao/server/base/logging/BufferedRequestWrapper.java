package com.baocao.server.base.logging;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class BufferedRequestWrapper extends HttpServletRequestWrapper
{
    private static final class BufferedServletInputStream extends ServletInputStream
    {
        private ByteArrayInputStream bais;

        public BufferedServletInputStream(ByteArrayInputStream bais)
        {
            this.bais = bais;
        }

        @Override
        public int available()
        {
            return this.bais.available();
        }

        @Override
        public int read()
        {
            return this.bais.read();
        }

        @Override
        public int read(byte[] buf, int off, int len)
        {
            return this.bais.read(buf, off, len);
        }

        @Override
        public boolean isFinished()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isReady()
        {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void setReadListener(ReadListener readListener)
        {
            // TODO Auto-generated method stub

        }

    }

    private byte[] mBodyBuffer;

    public BufferedRequestWrapper(HttpServletRequest request) throws IOException
    {
        super(request);

        InputStream in = request.getInputStream();
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead = -1;
        while ((bytesRead = in.read(buffer)) > 0)
        {
            baos.write(buffer, 0, bytesRead);
        }
        mBodyBuffer = baos.toByteArray();
    }

    public String getRequestBody()
    {
        return new String(mBodyBuffer, Charset.forName("UTF-8"));
    }

    @Override
    public BufferedReader getReader() throws IOException
    {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }

    @Override
    public ServletInputStream getInputStream()
    {
        ByteArrayInputStream in = new ByteArrayInputStream(mBodyBuffer);
        return new BufferedServletInputStream(in);
    }

}
