package com.baocao.server.base.util;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil
{
    private static Gson sGson;

    private final static GsonBuilder sGsonBuilder = new GsonBuilder();

    static
    {
        register();
    }

    private static void register()
    {
        sGson = sGsonBuilder.create();
    }

    public static Gson getGson()
    {
        return sGson;
    }

    public static void registerTypeAdapter(Type type, Object typeAdapter)
    {
        sGsonBuilder.registerTypeAdapter(type, typeAdapter);
        sGson = sGsonBuilder.create();
    }

    public static String toString(Object obj)
    {
        return sGson.toJson(obj);
    }

    public static <T> T toObject(String json, Class<T> typeClass)
    {
        return sGson.fromJson(json, typeClass);
    }

    public static <T> T toObject(String json, Type typeOfT)
    {
        return sGson.fromJson(json, typeOfT);
    }

    public static <T> List<T> toList(String json, Class<T> typeClass)
    {
        return sGson.fromJson(json, new ListOfJson<T>(typeClass));
    }
}
