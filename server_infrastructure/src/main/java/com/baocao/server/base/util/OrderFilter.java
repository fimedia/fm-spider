package com.baocao.server.base.util;

import javax.servlet.Filter;

/**
 * In order Filter
 * 
 * @author chenle
 *
 */
public interface OrderFilter extends Filter
{
    /**
     * Return filters' execution order
     * 
     * @return execution order
     */
    int getOrder();
}
