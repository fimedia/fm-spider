package com.baocao.server.base.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baocao.server.base.logging.BufferedRequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class CustomExceptionHandler
{
    private Logger mLogger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = CustomException.class)
    @ResponseBody
    public ErrorDesc handleCustomException(CustomException ex, HttpServletResponse response)
    {
        mLogger.error("Handling CustomExceptoin", ex);
        response.setStatus(ex.getHttpStatusCode());
        return new ErrorDesc(ex.getErrCode(), ex.getErrMsg());
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    @ResponseBody
    public ErrorDesc handleMissingServletRequestParameterException(MissingServletRequestParameterException ex, HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        logRequest(request);
        mLogger.error("Handling MissingServletRequestParameterException", ex);
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return new ErrorDesc(BasicErrorCode.E_BAD_REQUEST.getCode(), ex.getMessage());
    }

    @ExceptionHandler(value = HttpMessageConversionException.class)
    @ResponseBody
    public ErrorDesc handleHttpMessageNotReadableException(HttpMessageConversionException ex, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        logRequest(request);
        mLogger.error("Handling HttpMessageConversionException", ex);
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return new ErrorDesc(BasicErrorCode.E_BAD_REQUEST.getCode(), ex.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ErrorDesc handleUncaughtException(Exception ex, HttpServletRequest request, HttpServletResponse response) throws Exception
    {
        logRequest(request);
        mLogger.error("Handling Exception: ", ex);
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        return new ErrorDesc(BasicErrorCode.E_INTERNAL_SERVER_ERROR.getCode(), BasicErrorCode.E_INTERNAL_SERVER_ERROR.getDesc());
    }

    private void logRequest(HttpServletRequest request) throws IOException
    {
        BufferedRequestWrapper requestWrapper = new BufferedRequestWrapper(request);
        mLogger.error("Error request, url: {}, data: {}", request.getRequestURI(), requestWrapper.getRequestBody());
    }

}
