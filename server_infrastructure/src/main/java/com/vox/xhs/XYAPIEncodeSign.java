package com.vox.xhs;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class XYAPIEncodeSign {
    public static String signWithQueryItems(Map<String,String> params) {
        String key = params.get("deviceId");
        if (key == null) {
            return "";
        }
        String query = queryStringWithQueryItems(params);
        try {
            String encodedQuery = URLEncoder.encode(query, "UTF-8");
            String sign = signString(key, encodedQuery);
            String m = getMd5(sign);
            return getMd5(m+key);

        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    public static String queryStringWithQueryItems(Map<String,String> params) {
        Map<String, String> treeMap = new TreeMap<String, String>(params);
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String,String> e : treeMap.entrySet()) {
            sb.append(e.getKey()).append("=").append(e.getValue());
        }
        return sb.toString();
    }

    public static String signString(String key, String data) {
        byte[] dataBytes = data.getBytes();
        byte[] keyBytes = key.getBytes();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < dataBytes.length; i++) {
            int d = dataBytes[i];
            int k = keyBytes[i % keyBytes.length];
            int x = (d ^ k) & 0xff;
            sb.append(x);
        }
        return sb.toString();
    }

    private static String hexlify(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b: bytes) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }

    public static byte[] unhexlify(String argbuf) {
        int arglen = argbuf.length();
        if (arglen % 2 != 0)
            throw new RuntimeException("Odd-length string");

        byte[] retbuf = new byte[arglen/2];

        for (int i = 0; i < arglen; i += 2) {
            int top = Character.digit(argbuf.charAt(i), 16);
            int bot = Character.digit(argbuf.charAt(i+1), 16);
            if (top == -1 || bot == -1)
                throw new RuntimeException("Non-hexadecimal digit found");
            retbuf[i / 2] = (byte) ((top << 4) + bot);
        }
        return retbuf;
    }


    public static String getMd5(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            return hexlify(md.digest());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String getMd5(byte[] str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str);
            return hexlify(md.digest());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String getSha256(byte[] str) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(str);
            return hexlify(md.digest());

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static byte[] getMd5Digest(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            return md.digest();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decodeStrategy(String udid, byte[] strategy) {
        String result = "";
        byte[] md5 = getMd5Digest(udid);
        try {
            SecretKey securekey = new SecretKeySpec(Arrays.copyOfRange(md5, 2, 10), "DES");
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, securekey);
            byte[] r = cipher.doFinal(strategy);
            result = new String(r);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String S2(String data) {
        byte[] md5 = getMd5Digest(data);
        try {
            SecretKey securekey = new SecretKeySpec(Arrays.copyOfRange(md5, 2, 10), "DES");
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, securekey);
            byte[] r = cipher.doFinal(data.getBytes());
            return getMd5(r);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String S4(String data) {
        byte[] md5 = getMd5Digest(data);
        try {
            SecretKey securekey = new SecretKeySpec(Arrays.copyOfRange(md5, 2, 10), "DES");
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, securekey);
            byte[] r = cipher.doFinal(data.getBytes());
            return getSha256(r);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String Sign(String strategy, String toString) throws IOException {
        switch (strategy) {
            case "S2":
                return S2(toString);
            case "S4":
                return S4(toString);
        }
        throw new IOException("不支持的加密");
    }
}
