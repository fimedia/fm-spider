package com.vox.xhs;

public class Result<T> {
    private Boolean success;
    private int result;
    private String msg;
    private T data;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public T getData() {
        return data;
    }

    T checkData() throws ServerException {
        if (!isOk()) {
            throw new ServerException(result, msg);
        }
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private boolean isOk() {
        return result == 0 && success == true;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("com.vox.xhs.Result{");
        sb.append("success='").append(success).append('\'');
        sb.append(", result='").append(result).append('\'');
        sb.append(", data=").append(data);
        sb.append('}');
        return sb.toString();
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
