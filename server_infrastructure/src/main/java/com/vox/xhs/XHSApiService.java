package com.vox.xhs;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.Map;

public interface XHSApiService {
    @GET("/api/sns/v9/search/notes")
    Call<Result<JSONObject>> searchNotes(@QueryMap Map<String, String> params);
    @GET("/api/sns/v3/search/trending")
    Call<Result<JSONArray>>searchTrending(@QueryMap Map<String, String> params);
    @GET("/api/sns/v3/search/notes/recommend_info")
    Call<Result<JSONObject>> searchNotesRecommendInfo(@QueryMap Map<String, String> params);
    @GET("/api/sns/v2/search/user")
    Call<Result<JSONArray>> searchUser(@QueryMap Map<String, String> params);
    @GET("/api/store/ps/products/v3")
    Call<Result<JSONObject>> storePsProdcts(@QueryMap Map<String, String> params);
    @GET("/api/sns/v6/homefeed")
    Call<Result<JSONArray>> homefeed(@QueryMap Map<String, String> params);
    @GET("/api/sns/v3/user/{Id}/info")
    Call<Result<JSONObject>> userInfo(@Path("Id") String userId, @QueryMap Map<String, String> params);
    @GET("/api/sns/v3/note/user/{Id}")
    Call<Result<JSONObject>> noteUser(@Path("Id") String userId, @QueryMap Map<String, String> params);
    @POST("/api/sns/v1/user/activate")
    @FormUrlEncoded
    Call<Result<JSONObject>> userActivate(@FieldMap Map<String, String> params);
    @GET("/api/sns/v2/note/{Id}/feed")
    Call<Result<JSONArray>> noteFeed(@Path("Id") String feedId, @QueryMap Map<String, String> params);
}