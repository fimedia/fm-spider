package com.vox.xhs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class XHSApi {

    private XHSApiService userService;
    private String udid;
    private String idfa;
    private String idfv;
    private String fp;
    private String sessionId;
    private RetrofitClient retrofitClient;

    /**
     * V8视频API
     */
    public XHSApi() throws IOException, ServerException {
        udid = UUID.randomUUID().toString().toUpperCase();
        idfa = UUID.randomUUID().toString().toUpperCase();
        idfv = UUID.randomUUID().toString().toUpperCase();
        retrofitClient = new RetrofitClient();
        userService = retrofitClient.create(XHSApiService.class);

        String dateString = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        fp = dateString+RandomString.genHexString(32)+"00";
        String sig = XYAPIEncodeSign.getMd5("shumei_ios_sec_key_" + fp);
        fp = fp + sig.substring(0, 14);
        userActivate();
    }

    private Map<String,String> genCommonParam() {
        HashMap<String,String> params = new HashMap<>();
        params.put("deviceId", udid);
        params.put("device_fingerprint", fp);
        params.put("device_fingerprint1", fp);
        params.put("lang", "zh");
        params.put("platform", "iOS");
        params.put("sid", sessionId);
        Date date= new Date();
        params.put("t", String.valueOf(date.getTime()/1000));
        return params;
    }

    private <T> T doCall(Call<T> call) throws ServerException {
        Response<T> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
                return response.body();
            } else {
                if (response.errorBody() != null) {
                    JSONObject o = JSON.parseObject(response.errorBody().string());
                    int resultCode = o.getIntValue("result");
                    String msg = o.getString("msg");
                    String cryptMethod = o.getString("crypt_method");
                    if (resultCode == -200 && msg != null && msg.equals("参数错误") && cryptMethod != null) {
                        byte[] cryptMethodHex = XYAPIEncodeSign.unhexlify(cryptMethod);
                        String strategy = XYAPIEncodeSign.decodeStrategy(udid, cryptMethodHex);
//                        System.out.println(strategy);
                        retrofitClient.setStrategy(strategy);
                        msg += " udid: " + udid + ", strategy: " + strategy;
                        System.err.println(msg);
                        throw new CryptoException(resultCode, msg);
                    }
                    throw new ServerException(resultCode, msg);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 搜索笔记
     * @param keyword 关键字
     * @param page 页码
     * @param pageSize 页个数，默认20
     * @return 搜索结果
     * @throws IOException
     * @throws ServerException
     */
    public JSONObject searchNotes(String keyword, int page, int pageSize) throws IOException, ServerException {
            return searchNotes(keyword, page, pageSize, "general");
    }

    /**
     * 搜索笔记
     * @param keyword 关键字
     * @param page 页码
     * @param pageSize 页个数，默认20
     * @param sort general: 全部, popularity_descending: 最热, time_descending: 最新
     * @return
     * @throws IOException
     * @throws ServerException
     */
    public JSONObject searchNotes(String keyword, int page, int pageSize, String sort) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
        params.put("api_extra", "");
        params.put("allow_rewrite", "1");
        params.put("keyword", keyword);
        params.put("keyword_type", "normal");
        params.put("page", String.valueOf(page));
        params.put("page_pos", "0");
        params.put("page_size", String.valueOf(pageSize));
        params.put("sort", sort);
        params.put("source", "search_result_notes");
        params.put("search_id", RandomString.genHexString(32).toUpperCase());
        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));

        Call<Result<JSONObject>> call = userService.searchNotes(params);
        Result<JSONObject> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    /**
     * 城市热门
     * @param city 城市
     * @return 搜索结果
     * @throws IOException
     * @throws ServerException
     */
    public JSONArray searchTrending(String city) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
        params.put("city", city);
        params.put("source", "explore_feed");

        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONArray>> call = userService.searchTrending(params);
        Result<JSONArray> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    /**
     * 搜索结果推荐条目
     * @param keyword 关键词
     * @return 结果
     * @throws IOException
     * @throws ServerException
     */
    public JSONObject searchNotesRecommendInfo(String keyword) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
        params.put("api_extra", "");
        params.put("keyword", keyword);
        params.put("keyword_type", "local_history");
        params.put("source", "search_result_notes");
        params.put("search_id", RandomString.genHexString(32).toUpperCase());

        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONObject>> call = userService.searchNotesRecommendInfo(params);
        Result<JSONObject> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    /**
     * 搜索用户
     * @param keyword 关键词
     * @param page 页码
     * @param pageSize 个数
     * @return 用户列表
     * @throws IOException
     * @throws ServerException
     */
    public JSONArray searchUser(String keyword, int page, int pageSize) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
        params.put("keyword", keyword);
        params.put("page", String.valueOf(page));
        params.put("page_size", String.valueOf(pageSize));
        params.put("source", "search_result_notes");
        params.put("search_id", RandomString.genHexString(32).toUpperCase());

        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONArray>> call = userService.searchUser(params);
        Result<JSONArray> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    /**
     * 搜索商品
     * @param keyword 商品关键词
     * @param page 页码
     * @param pageSize 个数
     * @return 商品信息
     * @throws IOException
     * @throws ServerException
     */
    public JSONObject storePsProdcts(String keyword, int page, int pageSize) throws IOException, ServerException {
        return storePsProdcts(keyword, page, pageSize, null);
    }

    /**
     * 搜索商品
     * @param keyword 商品关键词
     * @param page 页码
     * @param pageSize 个数
     * @param sort null：综合，sales_qty：销量，new_arrival：上新, price_asc: 价格升, price_desc: 价格降
     * @return 商品信息
     * @throws IOException
     * @throws ServerException
     */
    public JSONObject storePsProdcts(String keyword, int page, int pageSize, String sort) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
        params.put("keyword", keyword);
        params.put("page", String.valueOf(page));
        params.put("size", String.valueOf(pageSize));
        params.put("source", "search_result_notes");
        params.put("search_id", RandomString.genHexString(32).toUpperCase());
        if (sort != null && !sort.isEmpty()) {
            params.put("sort", sort);
        }

        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONObject>> call = userService.storePsProdcts(params);
        Result<JSONObject> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    /**
     * 用户信息详情
     * @param userId 用户id
     * @return 用户信息详情
     * @throws IOException
     * @throws ServerException
     */
    public JSONObject userInfo(String userId) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());

        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONObject>> call = userService.userInfo(userId, params);
        Result<JSONObject> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    /**
     * 用户笔记列表
     * @param userId 用户id
     * @param videoOnly 是否过滤视频
     * @param page 页码
     * @param pageSize 个数
     * @return 笔记列表
     * @throws IOException
     * @throws ServerException
     */
    public JSONObject noteUser(String userId, boolean videoOnly, int page, int pageSize) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
        params.put("page", String.valueOf(page));
        params.put("page_size", String.valueOf(pageSize));
        if (videoOnly == true) {
            params.put("sub_tag_id", "special.video");
        }

        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONObject>> call = userService.noteUser(userId, params);
        Result<JSONObject> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    public JSONObject userActivate() throws IOException, ServerException {
        TreeMap<String, String> params = new TreeMap<>();
        params.put("deviceId", udid);
        params.put("device_fingerprint", fp);
        params.put("device_fingerprint1", fp);
        params.put("idfa", idfa);
        params.put("idfv", idfv);
        params.put("platform", "iOS");
        params.put("lang", "zh");
        params.put("t", String.valueOf(new Date().getTime()/1000));
        params.put("vitaminc", XYAPIEncodeSign.getMd5(udid+"0").toUpperCase());
        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONObject>> call = userService.userActivate(params);
        Result<JSONObject> result = null;
        try {
            result = doCall(call);
        } catch (CryptoException e) {
            call = call.clone();
            result = doCall(call);
        }
        if (result != null) {
            JSONObject activeResult = result.checkData();
            sessionId = "session." + activeResult.getString("session");
            return activeResult;
        } else {
            return null;
        }
    }

    /**
     * 首页信息流
     * @param noteIndex
     * @param num
     * @return
     * @throws IOException
     * @throws ServerException
     */
    public JSONArray homefeed(int noteIndex, int num) throws IOException, ServerException {
        return homefeed(noteIndex, num, null);
    }

    /**
     * 首页信息流
     * @param noteIndex
     * @param num
     * @return
     * @throws IOException
     * @throws ServerException
     */
    public JSONArray homefeed(int noteIndex, int num, String cursor) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
        params.put("note_index", String.valueOf(noteIndex));
        params.put("num", String.valueOf(num));
        params.put("refresh_type", "2");
        params.put("oid", "homefeed_recommend");
        params.put("trace_id", UUID.randomUUID().toString().toUpperCase());
        params.put("use_jpeg", "1");

        if (cursor != null && !cursor.isEmpty()) {
            params.put("cursor_score", cursor);
        }

        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONArray>> call = userService.homefeed(params);
        Result<JSONArray> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    /**
     * 获取笔记详情
     * @param noteId 笔记id
     * @return 笔记信息
     * @throws IOException
     * @throws ServerException
     */
    public JSONArray noteFeed(String noteId) throws IOException, ServerException {
        TreeMap<String,String> params = new TreeMap<>(genCommonParam());
//        params.put("source_note_id", noteId);
        params.put("source", "note_detail_r10");
        params.put("fetch_mode", "1");
        params.put("sign", XYAPIEncodeSign.signWithQueryItems(params));
        Call<Result<JSONArray>> call = userService.noteFeed(noteId, params);
        Result<JSONArray> result = doCall(call);
        if (result != null) {
            return result.checkData();
        } else {
            return null;
        }
    }

    public static void main(String[] args){
        System.out.println("Hello xhs!");
    }
}
