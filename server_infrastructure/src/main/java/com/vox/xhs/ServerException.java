package com.vox.xhs;

public class ServerException extends Exception {
    public int code;

    public ServerException(int code, String message) {
        super(message);
        this.code = code;
    }
}

