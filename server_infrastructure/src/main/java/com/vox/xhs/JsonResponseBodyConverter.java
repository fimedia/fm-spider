package com.vox.xhs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import okhttp3.ResponseBody;
import retrofit2.Converter;

import java.io.IOException;
import java.lang.reflect.Type;

public class JsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {

    private Type type;

    JsonResponseBodyConverter(Type type) {
        this.type = type;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        String body = value.string();
        JSONObject o = JSON.parseObject(body);
        return (T) JSON.parseObject(body, type);
    }
}