package com.vox.xhs;

import okhttp3.*;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class XHSClientInterceptor implements Interceptor {

    private String strategy;

    public XHSClientInterceptor() {
        strategy = "S2";
    }


    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        StringBuilder sb = new StringBuilder();
        HttpUrl url = originalRequest.url();
        TreeMap<String, String> formMap = new TreeMap<>();

        if (originalRequest.method().equals("GET")) {
            for (int i = 0; i < url.querySize(); i++) {
                formMap.put(url.queryParameterName(i), url.queryParameterValue(i));
            }
        } else {
            RequestBody body = originalRequest.body();
            if (body instanceof FormBody) {
                FormBody formBody = (FormBody) body;
                for (int i = 0; i < formBody.size(); i++) {
                    formMap.put(formBody.name(i), formBody.value(i));
                }
            }
        }

        formMap.put("url", url.uri().getPath());
        for (Map.Entry<String, String> entry : formMap.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue());
        }

        Request requestWithUserAgent = originalRequest.newBuilder()
            .header("User-Agent", "discover/5.46.2 (iPhone; iOS 9.2; Scale/2.00) Resolution/750*1334 Version/5.46.2 Build/5462005 Device/(Apple Inc.;iPhone7,2) NetType/WiFi")
            //.header("sid", "session.1554867454741374403931")
            .header("shield", XYAPIEncodeSign.Sign(strategy, sb.toString()))
            .header("Accept", "application/json")
            .build();
        return chain.proceed(requestWithUserAgent);
    }

    public void setStrategy(String s) {
        strategy = s;
    }
}
