package com.vox.xhs;

public class CryptoException extends ServerException {
    public CryptoException(int code, String message) {
        super(code, message);
        this.code = code;
    }
}
