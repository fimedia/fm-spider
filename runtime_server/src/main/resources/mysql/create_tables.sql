create schema note;

use note;
    create table hibernate_sequence (
       next_val bigint
    ) engine=MyISAM
    insert into hibernate_sequence values ( 1 )
    insert into hibernate_sequence values ( 1 )
    create table spider_note_entity (
       id bigint not null,
        comment varchar(255),
        content longtext,
        like_count varchar(255),
        note_id varchar(255),
        pic_files_json longtext,
        pic_urls_json longtext,
        star varchar(255),
        title longtext,
        user_id varchar(255),
        web_page_url varchar(4096),
        primary key (id)
    ) engine=MyISAM

    create table spider_note_user_entity (
       id bigint not null,
        image_url varchar(4096),
        nick_name varchar(255),
        total_count integer not null,
        user_id varchar(255),
        primary key (id)
    ) engine=MyISAM