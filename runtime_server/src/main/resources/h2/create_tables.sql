
    create table spider_note_entity (
       id bigint not null,
        comment varchar(255),
        content clob,
        like_count varchar(255),
        note_id varchar(255),
        pic_files_json clob,
        pic_urls_json clob,
        star varchar(255),
        title clob,
        user_id varchar(255),
        web_page_url varchar(4096),
        primary key (id)
    )

    create table spider_note_user_entity (
       id bigint not null,
        image_url varchar(4096),
        nick_name varchar(255),
        total_count integer not null,
        user_id varchar(255),
        primary key (id)
    )