package com.baocao.business.model.spider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DownloadSociollaCommentEntityDAO extends JpaRepository<DownloadSociollaCommentEntity,Long> {

    DownloadSociollaCommentEntity findByCommentIdAndProductId(String commentId,String productId);
}
