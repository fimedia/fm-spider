package com.baocao.business.model.spider;

import com.baocao.server.base.util.JsonUtil;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity(name="spider_note_entity")
public class NoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Lob
    @Column
    private String title;
    @Lob
    @Column
    private String content;

    @Transient
    private List<String> picUrls;
    @Transient
    private List<String> picFiles;

    @Lob
    @Column
    private String picUrlsJson;

    @Lob
    @Column
    private String picFilesJson;

    @Transient
    private List<String> videoUrls;
    @Transient
    private List<String> vidoeFiles;

    private String likeCount;
    private String comment;
    private String star;

    @Column
    @Size(max=4096)
    private String webPageUrl;

    private String userId;

    private String noteId;

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Transient
    public List<String> getPicUrls() {
        if (picUrls == null && !StringUtils.isEmpty(picUrlsJson)) {
            picUrls = JsonUtil.toList(picUrlsJson, String.class);
        }
        return picUrls;
    }

    public void setPicUrls(List<String> picUrls) {
        this.picUrls = picUrls;
        picUrlsJson = JsonUtil.toString(picUrls);
    }

    public String getPicUrlsJson() {
        return picUrlsJson;
    }

    public void setPicUrlsJson(String picUrlsJson) {
        this.picUrlsJson = picUrlsJson;
    }

    @Transient
    public List<String> getPicFiles() {
        if (picFiles == null && !StringUtils.isEmpty(picFilesJson)) {
            picFiles = JsonUtil.toList(picFilesJson, String.class);
        }
        return picFiles;
    }

    public void setPicFiles(List<String> picFiles) {
        this.picFiles = picFiles;
        picFilesJson = JsonUtil.toString(picFiles);
    }

    public String getPicFilesJson() {
        return picFilesJson;
    }

    public void setPicFilesJson(String picFilesJson) {
        this.picFilesJson = picFilesJson;
    }

    @Transient
    public List<String> getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    @Transient
    public List<String> getVidoeFiles() {
        return vidoeFiles;
    }

    public void setVidoeFiles(List<String> vidoeFiles) {
        this.vidoeFiles = vidoeFiles;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String like) {
        this.likeCount = like;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getWebPageUrl() {
        return webPageUrl;
    }

    public void setWebPageUrl(String webPageUrl) {
        this.webPageUrl = webPageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
