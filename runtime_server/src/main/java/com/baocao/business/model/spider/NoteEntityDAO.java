package com.baocao.business.model.spider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteEntityDAO extends JpaRepository<NoteEntity, Long> {

    NoteEntity findNoteEntityByNoteId(String noteId);
    List<NoteEntity> findNoteEntityByUserId(String userId);
}
