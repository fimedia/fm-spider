package com.baocao.business.model.spider;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "spider_product_comment_item")
public class DownloadSociollaCommentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "comment_id")
    private String commentId;

    @Column(name = "comment")
    private String comment;

    @Column(name = "ranting_info")
    private String rantingInfo;

    @Column(name = "orig_url")
    private String origUrl;

    @Column(name = "source")
    private String source;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRantingInfo() {
        return rantingInfo;
    }

    public void setRantingInfo(String rantingInfo) {
        this.rantingInfo = rantingInfo;
    }

    public String getOrigUrl() {
        return origUrl;
    }

    public void setOrigUrl(String origUrl) {
        this.origUrl = origUrl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
