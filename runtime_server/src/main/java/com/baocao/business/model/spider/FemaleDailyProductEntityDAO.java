package com.baocao.business.model.spider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FemaleDailyProductEntityDAO extends JpaRepository<FemaleDailyProductEntity, Long> {

    List<FemaleDailyProductEntity> findProductByBrand(@Param("brand") String brand);

    @Query(value = "select c from spider_female_daily_product c order by c.review desc")
    List<FemaleDailyProductEntity> getProductsByReviewDesc();

    List<FemaleDailyProductEntity> findProductByWebPageUrl(@Param("webPageUrl") String webPageUrl);
}
