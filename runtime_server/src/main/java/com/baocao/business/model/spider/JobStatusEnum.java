package com.baocao.business.model.spider;

public enum JobStatusEnum {
    CREATED(1), STARTED(2), FINISHED(3);

    JobStatusEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    private int type;

}
