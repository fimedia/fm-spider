package com.baocao.business.model.spider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DownloadItemEntityDAO extends JpaRepository<DownloadItemEntity, Long> {

    DownloadItemEntity findByTypeAndItemId(int type, String itemId);
}
