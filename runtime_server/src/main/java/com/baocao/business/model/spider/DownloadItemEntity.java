package com.baocao.business.model.spider;

import com.baocao.server.base.util.JsonUtil;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity(name="spider_download_item")
public class DownloadItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    @Size(max=4096)
    private String webPageUrl;

    @Column
    private String itemId;

    @Column
    private int type;

    @Lob
    @Column
    private String title;
    @Lob
    @Column
    private String content;

    @Transient
    private List<String> downloadUrls;
    @Transient
    private List<String> downloadFiles;

    @Lob
    @Column
    private String downloadUrlsJson;

    @Lob
    @Column
    private String downloadFilesJson;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWebPageUrl() {
        return webPageUrl;
    }

    public void setWebPageUrl(String webPageUrl) {
        this.webPageUrl = webPageUrl;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getDownloadUrls() {
        if (downloadUrls == null && !StringUtils.isEmpty(downloadUrlsJson)) {
            downloadUrls = JsonUtil.toList(downloadUrlsJson, String.class);
        }
        return downloadUrls;
    }

    public void setDownloadUrls(List<String> downloadUrls) {
        this.downloadUrls = downloadUrls;
        downloadUrlsJson = JsonUtil.toString(downloadUrls);
    }

    public List<String> getDownloadFiles() {
        if (downloadFiles == null && !StringUtils.isEmpty(downloadFilesJson)) {
            downloadFiles = JsonUtil.toList(downloadFilesJson, String.class);
        }
        return downloadFiles;
    }

    public void setDownloadFiles(List<String> downloadFiles) {
        this.downloadFiles = downloadFiles;
        downloadFilesJson = JsonUtil.toString(downloadFiles);
    }

    public String getDownloadUrlsJson() {
        return downloadUrlsJson;
    }

    public void setDownloadUrlsJson(String downloadUrlsJson) {
        this.downloadUrlsJson = downloadUrlsJson;
    }

    public String getDownloadFilesJson() {
        return downloadFilesJson;
    }

    public void setDownloadFilesJson(String downloadFilesJson) {
        this.downloadFilesJson = downloadFilesJson;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
