package com.baocao.business.model.spider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DownloadSociollaltemEntityDAO extends JpaRepository<DownloadSociollaltemEntity, Long> {

    DownloadSociollaltemEntity findByOrigId(String origId);
}
