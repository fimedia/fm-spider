package com.baocao.business.model.spider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteUserEntityDAO extends JpaRepository<NoteUserEntity, Long> {

    NoteUserEntity findUserByUserId(@Param("userId") String userId);
}
