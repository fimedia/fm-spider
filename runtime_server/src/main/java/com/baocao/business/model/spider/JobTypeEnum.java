package com.baocao.business.model.spider;

public enum JobTypeEnum {
    NOTE_FETCH(1), USER_FETCH(2), KEYWORD_FETCH(3), URL_FETCH(4), API_FETCH(5);

    JobTypeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    private int type;
}
