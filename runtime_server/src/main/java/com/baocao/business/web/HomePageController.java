package com.baocao.business.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomePageController {
    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String index(){
        return "index";
    }

    @RequestMapping(value = "/crawl",method = RequestMethod.GET)
    public String crawl(){
        return "crawl";
    }

    @RequestMapping(value = "/notes",method = RequestMethod.GET)
    public String notes(){
        return "notes";
    }

    @RequestMapping(value = "/user",method = RequestMethod.GET)
    public String user(){
        return "user";
    }

    @RequestMapping(value = "/download",method = RequestMethod.GET)
    public String download(){
        return "download";
    }

    @RequestMapping(value = "/crawl_page",method = RequestMethod.GET)
    public String crawlPage(){
        return "crawl_page";
    }
}