package com.baocao.business.protocol.sociolla;

import com.alibaba.fastjson.annotation.JSONField;

public class SociollaProductRatingInfo {

    @JSONField(name = "review_count")
    private Long reviewCount;

    @JSONField(name = "pigmentation")
    private Double pigmentation;

    @JSONField(name = "long_wear")
    private Double longWear;

    @JSONField(name = "packaging")
    private Double packaging;

    @JSONField(name = "texture")
    private Double texture;

    @JSONField(name = "value_for_money")
    private Double valueForMoney;

    @JSONField(name = "rate_value")
    private Double rateValue;

    public Long getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Long reviewCount) {
        this.reviewCount = reviewCount;
    }

    public Double getPigmentation() {
        return pigmentation;
    }

    public void setPigmentation(Double pigmentation) {
        this.pigmentation = pigmentation;
    }

    public Double getLongWear() {
        return longWear;
    }

    public void setLongWear(Double longWear) {
        this.longWear = longWear;
    }

    public Double getPackaging() {
        return packaging;
    }

    public void setPackaging(Double packaging) {
        this.packaging = packaging;
    }

    public Double getTexture() {
        return texture;
    }

    public void setTexture(Double texture) {
        this.texture = texture;
    }

    public Double getValueForMoney() {
        return valueForMoney;
    }

    public void setValueForMoney(Double valueForMoney) {
        this.valueForMoney = valueForMoney;
    }

    public Double getRateValue() {
        return rateValue;
    }

    public void setRateValue(Double rateValue) {
        this.rateValue = rateValue;
    }
}
