package com.baocao.business.protocol.base.data;

public abstract class BaseDataInfo {

    private String userId;

    private GameInfo game;

    private ClientInfo client;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public GameInfo getGame() {
        return game;
    }

    public void setGame(GameInfo game) {
        this.game = game;
    }

    public ClientInfo getClient() {
        return client;
    }

    public void setClient(ClientInfo client) {
        this.client = client;
    }
}
