package com.baocao.business.protocol.base;

public class Body {
    public String data;

    public Body(){
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
