package com.baocao.business.protocol.base;

public class Header {
    public final static String VERSION = "1.0.0";
    public final static String FORMAT_JSON = "0";
    public final static String ENCRPTY_NO = "0";
    public final static String COMPRESS_NO = "0";

    public String version;
    public String format;
    //TODO: encrypt?
    public String encrpty;
    public String compress;

    public Header(){
    }

    public final static Header defaultHeader(){
        Header header = new Header();
        header.version = VERSION;
        header.format = FORMAT_JSON;
        header.encrpty = ENCRPTY_NO;
        header.compress = COMPRESS_NO;

        return header;
    }


}
