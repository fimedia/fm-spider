package com.baocao.business.protocol.sociolla;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;
import java.util.Map;

public class SociollaProductCommentInfo {

    @JSONField(name = "comment_id")
    private String commentId;

    @JSONField(name = "comment")
    private String comment;

    @JSONField(name = "images")
    private List<String> images;

    @JSONField(name = "ranting_info")
    private Map<String,Object> rantingInfo;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public Map<String, Object> getRantingInfo() {
        return rantingInfo;
    }

    public void setRantingInfo(Map<String, Object> rantingInfo) {
        this.rantingInfo = rantingInfo;
    }
}
