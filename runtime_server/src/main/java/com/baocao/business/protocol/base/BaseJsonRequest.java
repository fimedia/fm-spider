package com.baocao.business.protocol.base;

import com.baocao.business.protocol.base.data.ClientInfo;
import com.baocao.business.protocol.base.data.GameInfo;

public class BaseJsonRequest {

    private Header header;

    private Body body;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    //    private String id;
//    private GameEntity game;
//    private ClientInfo client;
//
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public GameEntity getGame() {
//        return game;
//    }
//
//    public void setGame(GameEntity game) {
//        this.game = game;
//    }
//
//    public ClientInfo getClient() {
//        return client;
//    }
//
//    public void setClient(ClientInfo client) {
//        this.client = client;
//    }
}
