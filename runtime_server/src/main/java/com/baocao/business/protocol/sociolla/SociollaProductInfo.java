package com.baocao.business.protocol.sociolla;

import com.alibaba.fastjson.annotation.JSONField;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class SociollaProductInfo {

    @JSONField(name = "name")
    private String name;

    @JSONField(name = "brand_info")
    private Map<String,Object> brandInfo;

    @JSONField(name = "description")
    private String description;

    @JSONField(name = "covers")
    private List<String> covers;

    @JSONField(name = "specifications")
    private List<Map<String,Object>> specifications;

    @JSONField(name = "product_rating_info")
    private SociollaProductRatingInfo productRatingInfo;

    @JSONField(name = "category")
    private List<String> category;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getBrandInfo() {
        return brandInfo;
    }

    public void setBrandInfo(Map<String, Object> brandInfo) {
        this.brandInfo = brandInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public List<Map<String, Object>> getSpecifications() {
        return specifications;
    }

    public void setSpecifications(List<Map<String, Object>> specifications) {
        this.specifications = specifications;
    }


    public List<String> getCovers() {
        return covers;
    }

    public void setCovers(List<String> covers) {
        this.covers = covers;
    }

    public SociollaProductRatingInfo getProductRatingInfo() {
        return productRatingInfo;
    }

    public void setProductRatingInfo(SociollaProductRatingInfo productRatingInfo) {
        this.productRatingInfo = productRatingInfo;
    }
}
