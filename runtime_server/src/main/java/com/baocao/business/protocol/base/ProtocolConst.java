package com.baocao.business.protocol.base;

public class ProtocolConst {

    public static int CODE_SUCCESS = 200;
    public static int CODE_FAILED = 1;

    public static int EX_CODE_DEFAULT = 10000;


    public static String MSG_SUCCESS = "success";
    public static String MSG_FAILED = "failed";
}
