package com.baocao.business.protocol.base;

import com.baocao.business.protocol.base.data.StateInfo;

public class BaseJsonResponse {
    private StateInfo state;

    private Header header;

    private Body body;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public StateInfo getState() {
        return state;
    }

    public void setState(StateInfo state) {
        this.state = state;
    }
}
