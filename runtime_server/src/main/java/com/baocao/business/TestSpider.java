package com.baocao.business;

import com.baocao.business.service.spider.FemaleDailySpider;
import com.baocao.business.service.spider.NoteSpider;
import com.baocao.business.service.spider.pipeline.NoteSavePipeline;
import com.baocao.business.service.spider.pipeline.PicSavePipeline;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import java.util.Arrays;

public class TestSpider {

    public static void main(String[] args) {
        Spider spider = Spider.create(new FemaleDailySpider())
//                .addPipeline(new PicSavePipeline())
//                .addPipeline(new NoteSavePipeline())
                .thread(5);
        spider.startUrls(Arrays.asList("https://reviews.femaledaily.com/products/lips/lip-palette/wardah/lip-palette?tab=reviews&cat=&cat_id=0&age_range=&skin_type=&skin_tone=&skin_undertone=&hair_texture=&hair_type=&order=newest&page=1"));

        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
//        httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy("http-proxy-t1.dobel.cn",9180,"CNHAPPIERFIG34QRO0","6bgCAKru")));
        spider.setDownloader(httpClientDownloader);

        spider.start();

//        spider.addRequest()
        //.run();
    }
}
