package com.baocao.business.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SpiderConf {

    @Value("${spider.rootpath}")
    private String rootPath;

    @Value("${spider.noteprefix}")
    private String notePrefix;

    @Value("${spider.threadnum}")
    private int spiderThreadNumber;

    @Value("${spider.proxyurl}")
    private String proxyUrl;

    @Value("${spider.proxyusername}")
    private String proxyUserName;

    @Value("${spider.proxypassword}")
    private String proxyPassword;


    @Value("${spider.note_url_pattern}")
    private String noteUrlPattern;

    @Value("${spider.user_url_pattern}")
    private String userUrlPattern;

    @Value("${spider.ins_url_pattern}")
    private String insUrlPattern;

    @Value("${spider.pinterest_url_pattern}")
    private String pinterestUrlPattern;

    @Value("${spider.read_female_url_files}")
    private String femaleUrlFile;

    @Value("${spider.webdriver_chrome_driver_path}")
    private String webDriverChromeDriverPath;

    public String getRootPath() {
        return rootPath;
    }

    public void setRootPath(String rootPath) {
        this.rootPath = rootPath;
    }

    public String getNotePrefix() {
        return notePrefix;
    }

    public void setNotePrefix(String notePrefix) {
        this.notePrefix = notePrefix;
    }

    public String getProxyUrl() {
        return proxyUrl;
    }

    public void setProxyUrl(String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    public String getProxyUserName() {
        return proxyUserName;
    }

    public void setProxyUserName(String proxyUserName) {
        this.proxyUserName = proxyUserName;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    public int getSpiderThreadNumber() {
        return spiderThreadNumber;
    }

    public void setSpiderThreadNumber(int spiderThreadNumber) {
        this.spiderThreadNumber = spiderThreadNumber;
    }

    public String getNoteUrlPattern() {
        return noteUrlPattern;
    }

    public void setNoteUrlPattern(String noteUrlPattern) {
        this.noteUrlPattern = noteUrlPattern;
    }

    public String getUserUrlPattern() {
        return userUrlPattern;
    }

    public void setUserUrlPattern(String userUrlPattern) {
        this.userUrlPattern = userUrlPattern;
    }

    public String getFemaleUrlFile() {
        return femaleUrlFile;
    }

    public void setFemaleUrlFile(String femaleUrlFile) {
        this.femaleUrlFile = femaleUrlFile;
    }

    public String getInsUrlPattern() {
        return insUrlPattern;
    }

    public void setInsUrlPattern(String insUrlPattern) {
        this.insUrlPattern = insUrlPattern;
    }

    public String getPinterestUrlPattern() {
        return pinterestUrlPattern;
    }

    public void setPinterestUrlPattern(String pinterestUrlPattern) {
        this.pinterestUrlPattern = pinterestUrlPattern;
    }

    public String getWebDriverChromeDriverPath() {
        return webDriverChromeDriverPath;
    }

    public void setWebDriverChromeDriverPath(String webDriverChromeDriverPath) {
        this.webDriverChromeDriverPath = webDriverChromeDriverPath;
    }

}
