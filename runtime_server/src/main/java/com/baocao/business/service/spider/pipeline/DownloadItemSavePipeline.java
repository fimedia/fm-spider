package com.baocao.business.service.spider.pipeline;


import com.baocao.business.service.DownloadItemService;
import com.baocao.business.service.spider.model.DownloadItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

@Component("DownloadItemSavePipeline")
public class DownloadItemSavePipeline implements Pipeline {

    @Autowired
    private DownloadItemService service;

    @Override
    public void process(ResultItems resultItems, Task task) {
        DownloadItem item = resultItems.get("item");

        service.saveDownloadItem(item);
    }
}
