package com.baocao.business.service.spider.model;

public enum DownloadType {
    INSTAGRAM(1), PINTEREST(2);

    private int value;

    DownloadType(int v) {
        this.value = v;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static DownloadType valueFrom(int i) {
        DownloadType[] types = values();
        for (DownloadType type:types) {
            if (type.value == i) {
                return type;
            }
        }
        return null;
    }
}
