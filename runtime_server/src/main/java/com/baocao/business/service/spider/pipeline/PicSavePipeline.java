package com.baocao.business.service.spider.pipeline;

import com.baocao.business.service.NoteService;
import com.baocao.business.service.spider.model.Note;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rollin on 2016/1/11.
 */

@Component
public class PicSavePipeline implements Pipeline {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private NoteService noteService;

    @Override
    public void process(ResultItems resultItems, Task task) {
        Note note = resultItems.get("note");

        List<String> picFiles = new ArrayList<>();
        resultItems.put("pic_files", picFiles);
        noteService.downloadUrlToFiles(note, note.picUrls, picFiles, ".jpg", true);

        List<String> videoFiles = new ArrayList<>();
        resultItems.put("video_files", videoFiles);
        noteService.downloadUrlToFiles(note, note.videoUrls, videoFiles, ".mp4", true);
    }

}
