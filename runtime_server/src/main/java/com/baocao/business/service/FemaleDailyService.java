package com.baocao.business.service;


import com.baocao.business.conf.SpiderConf;
import com.baocao.business.model.spider.FemaleDailyProductEntity;
import com.baocao.business.model.spider.FemaleDailyProductEntityDAO;
import com.baocao.business.service.spider.FemaleDailySpider;
import com.baocao.business.service.spider.NoteSpider;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

@Component
public class FemaleDailyService {

    private static Logger logger = LoggerFactory.getLogger(FemaleDailyService.class);

    @Autowired
    private FemaleDailyProductEntityDAO dao;

    private Spider spiderInstance;

    @Autowired
    private FemaleDailySpider spiter;


    @Autowired
    private SpiderConf mConf;

    private  Spider getSpider() {
        synchronized(this) {
            if (spiderInstance == null) {
                spiderInstance = Spider.create(spiter)
                        .setExitWhenComplete(false)
                        .thread(mConf.getSpiderThreadNumber());
                HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
                httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy(mConf.getProxyUrl(),9180,mConf.getProxyUserName(),mConf.getProxyPassword())));
                spiderInstance.setDownloader(httpClientDownloader);
            }
        }

        return spiderInstance;
    }


    public void submitJob() {

//        String femaleUrlFile = mConf.getFemaleUrlFile();
//        File urlFile = new File(femaleUrlFile);
//
//        if (urlFile.exists()) {
//            BufferedReader reader = null;
//            try {
//                reader = new BufferedReader(new FileReader(urlFile));
//
//                String line = reader.readLine();
//                while (line!=null) {
//                    if (!StringUtils.isEmpty(line)) {
//                        Spider spider = getSpider();
//                        spider.addUrl(line);
//                    }
//                    line = reader.readLine();
//                }
//            } catch(Exception e) {
//                logger.error("error reading url file", e);
//            }finally {
//                IOUtils.closeQuietly(reader);
//            }
//
//
//        }else {
//            throw new IllegalStateException("file not exist, please check." + femaleUrlFile);
//        }

        String [] urls = new String[]
                {
                        "https://femaledaily.com/category/skincare",
                        "https://femaledaily.com/category/make-up",
                        "https://femaledaily.com/category/body",
                        "https://femaledaily.com/category/hair",
                        "https://femaledaily.com/category/fragrance",
                        "https://femaledaily.com/category/nails",
                        "https://femaledaily.com/category/tools"
                };

//        urls = new String[] {
//                "https://reviews.femaledaily.com/treatment-color/mist?brand=&order=popular&page=39"
//        };

        Spider spider = getSpider();
        for (String url: urls) {
            spider.addUrl(url);
        }

        if (spider.getStatus() == Spider.Status.Init || spider.getStatus() == Spider.Status.Stopped) {
            spider.start();
        }
    }

//    private void startUrl(List<String> startUrls) {
//        Spider spider = getSpider();
//        spider.addUrl(startUrls.toArray(new String[startUrls.size()]));
//        if (spider.getStatus() == Spider.Status.Init || spider.getStatus() == Spider.Status.Stopped) {
//            spider.start();
//        }
//    }

    public List<FemaleDailyProductEntity> listAll() {
        return dao.findAll();
    }


    public List<FemaleDailyProductEntity> getProductsByReview() {
        return dao.getProductsByReviewDesc();
    }

    public List<FemaleDailyProductEntity> findByBrand(String brand) {
        return dao.findProductByBrand(brand);
    }
}
