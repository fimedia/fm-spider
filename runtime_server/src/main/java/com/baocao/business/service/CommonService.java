package com.baocao.business.service;

import com.baocao.business.conf.SpiderConf;
import com.baocao.server.base.compress.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

@Component
public class CommonService {

    @Autowired
    private SpiderConf mConf;

    private String getRootFilePath() {
        return mConf.getRootPath();
    }

    public void downloadFolderAsZipFile(HttpServletRequest request, HttpServletResponse response, String zipNamePrefix, String targetFilePath) {
        try {
            String zipName = zipNamePrefix + "_" + UUID.randomUUID().toString()+".zip";
            String outFilePath = request.getSession().getServletContext().getRealPath("/");
            File fileZip = new File(outFilePath + "/" +zipName);

            ArrayList<File> inputFiles = IOUtils.getFiles(targetFilePath);
            FileOutputStream outStream = new FileOutputStream(fileZip);
            ZipOutputStream toClient = new ZipOutputStream(outStream);
            IOUtils.zipFile(getRootFilePath(), inputFiles,toClient);
            toClient.close();
            outStream.close();
            IOUtils.downloadFile(fileZip,response,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
