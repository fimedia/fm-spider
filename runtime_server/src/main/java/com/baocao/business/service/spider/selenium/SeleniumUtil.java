package com.baocao.business.service.spider.selenium;

import com.baocao.business.conf.SpiderConf;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import java.util.concurrent.TimeUnit;


@Component
public class SeleniumUtil {

    @Autowired
    private SpiderConf mConf;

    public WebDriver getUrl(String url, String waitPageFinishedElement, boolean headless){
        // 打开指定的网站
        // chromedriver服务地址
        System.setProperty("webdriver.chrome.driver", mConf.getWebDriverChromeDriverPath());
        // 新建一个WebDriver 的对象，但是new 的是谷歌的驱动
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(headless);

        Proxy proxy = new Proxy();
        proxy.setHttpProxy(mConf.getProxyUrl()+":9180");
        proxy.setSocksUsername(mConf.getProxyUserName());
        proxy.setSocksPassword(mConf.getProxyPassword());

        options.setProxy(proxy);
        WebDriver driver = new ChromeDriver(options);

        driver.manage().timeouts().pageLoadTimeout(3, TimeUnit.MINUTES);
        driver.get(url);

        if (!StringUtils.isEmpty(waitPageFinishedElement)) {
            WebDriverWait wait=new WebDriverWait(driver,10, 5000);
            wait.until(ExpectedConditions.presenceOfElementLocated(
                    By.xpath(waitPageFinishedElement)));
        }

        return driver;

    }

    public void quit(WebDriver driver) {
        if (driver!= null) {
            driver.quit();
        }
    }
}
