package com.baocao.business.service.spider;

import com.baocao.business.service.spider.model.Note;
import com.baocao.business.service.spider.selenium.SeleniumUtil;
import com.baocao.server.base.util.RegexUtil;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class NoteSpider extends AbstracePageProcessor {

    private Logger logger = LoggerFactory.getLogger(NoteSpider.class);

    public Pattern noteUrlPattern;

    public Pattern userUrlPattern;

    @Autowired
    private SeleniumUtil seleniumUtil;

    @Override
    public void process(Page page) {
        String url = page.getUrl().get();

        WebDriver driver = seleniumUtil.getUrl(url, "//meta[contains(@property, 'og:title')]", true);
        try {
            Note note = new Note();

            page.setHtml(new Html(driver.getPageSource(), url));
            Html html = page.getHtml();


            Selectable titleSelector = html.css("head > title", "text");
            String title = titleSelector.get();
            note.title = title;

            Selectable contentSelector = html.css(".left-card").css(".content").xpath("p/text()");
            List<String> lines = contentSelector.all();
            StringBuilder sb = new StringBuilder();
            for (String line : lines) {
                sb.append(line).append("\n");
            }
            String content = sb.toString();
            note.content = content;

            Selectable picSelector = html.css(".left-card").css(".slide").css(".inner", "style");
            List<String> picList = picSelector.all();
            note.picUrls = new ArrayList<>();
            for (String pic : picList) {
                String http = pic.replace("background-image: url(\"", "http:").replace("\");", "");
                note.picUrls.add(http);
            }

            Selectable videoObject = html.xpath("//video[contains(@class, 'videocontent')]/@src");
            if (videoObject != null) {
                String videoUrl = videoObject.toString();

                logger.debug("fetch video list: " + videoUrl);
                note.videoUrls  = new ArrayList<>();


                note.videoUrls.add(videoUrl);
            }

            Selectable operationBlock = html.css(".operation-block");
            String interactXpath = "/span/span/text()";

            Selectable likeSelector = operationBlock.css(".like").xpath(interactXpath);
            note.like = likeSelector.get();

            Selectable commentSelector = operationBlock.css(".comment").xpath(interactXpath);
            note.comment = commentSelector.get();

            Selectable starSelector = operationBlock.css(".star").xpath(interactXpath);
            note.star = starSelector.get();


            note.webPageUrl = page.getUrl().toString();
            note.noteId =  RegexUtil.extract(noteUrlPattern, page.getUrl().toString());

            Selectable userLink = html.xpath("//div[contains(@itemprop, 'author')]/meta[contains(@itemprop, 'url')]/@content");
            if (userLink!=null) {
                note.userId = RegexUtil.extract(userUrlPattern, userLink.get());
            }
            page.putField("note", note);

            logger.debug("fetch from web page, note:" + note.toString());
        }finally {
            seleniumUtil.quit(driver);
        }
    }

}
