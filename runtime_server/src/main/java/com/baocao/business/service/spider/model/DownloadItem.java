package com.baocao.business.service.spider.model;

import java.util.ArrayList;
import java.util.List;

public class DownloadItem {

    private List<String> downloadUrls = new ArrayList<>();

    private List<String> downloadFiles = new ArrayList<>();

    private DownloadType type;

    private String itemId;

    private String title;

    private String content;

    private String pageUrl;

    public List<String> getDownloadUrls() {
        return downloadUrls;
    }

    public void setDownloadUrls(List<String> downloadUrls) {
        this.downloadUrls = downloadUrls;
    }

    public List<String> getDownloadFiles() {
        return downloadFiles;
    }

    public void setDownloadFiles(List<String> downloadFiles) {
        this.downloadFiles = downloadFiles;
    }

    public DownloadType getType() {
        return type;
    }

    public void setType(DownloadType type) {
        this.type = type;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
