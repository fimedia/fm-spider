package com.baocao.business.service;

import com.baocao.business.conf.SpiderConf;
import com.baocao.business.model.spider.DownloadItemEntity;
import com.baocao.business.model.spider.DownloadItemEntityDAO;
import com.baocao.business.protocol.note.FetchResult;
import com.baocao.business.service.spider.PinterestSpider;
import com.baocao.business.service.spider.model.DownloadItem;
import com.baocao.business.service.spider.model.DownloadType;
import com.baocao.business.service.spider.pipeline.DownloadItemPipeline;
import com.baocao.business.service.spider.pipeline.DownloadItemSavePipeline;
import com.baocao.business.service.spider.pipeline.TargetPathHelper;
import com.baocao.server.base.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class DownloadItemService {

    private static Logger logger = LoggerFactory.getLogger(DownloadItemService.class);

    @Autowired
    private DownloadItemEntityDAO dao;

    @Autowired
    private SpiderConf mConf;

    private Spider pinterestSpiderInstance;

    @Autowired
    private PinterestSpider pinterestSpider;

    @Autowired
    private DownloadItemPipeline downloadItemPipeline;

    @Autowired
    private DownloadItemSavePipeline downloadItemSavePipeline;

    private  Spider getSpider() {
        synchronized(this) {
            if (pinterestSpiderInstance == null) {
                pinterestSpiderInstance = Spider.create(pinterestSpider)
                        .addPipeline(downloadItemPipeline)
                        .addPipeline(downloadItemSavePipeline)
                        .setExitWhenComplete(false)
                        .thread(mConf.getSpiderThreadNumber());
                HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
                httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy("127.0.0.1", 1087)));
//                httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy(mConf.getProxyUrl(),9180,mConf.getProxyUserName(),mConf.getProxyPassword())));
                pinterestSpiderInstance.setDownloader(httpClientDownloader);
            }
        }

        return pinterestSpiderInstance;
    }

    private void startFetchUrl(List<String> startUrls) {
        Spider spider = getSpider();
        spider.addUrl(startUrls.toArray(new String[startUrls.size()]));
        if (spider.getStatus() == Spider.Status.Init || spider.getStatus() == Spider.Status.Stopped) {
            spider.start();
        }
    }

    public FetchResult fetchByUrl(String url, boolean force) {
        FetchResult result = new FetchResult();

        Pattern pattern = Pattern.compile(mConf.getPinterestUrlPattern());

        String downloadItemId = RegexUtil.extract(pattern, url);
        if (!StringUtils.isEmpty(downloadItemId)) {
            startFetchUrl(Arrays.asList(url));
            result.setSuccess(true);
            return result;
        } else {
            Pattern insPattern = Pattern.compile(mConf.getInsUrlPattern());
            String insId = RegexUtil.extract(insPattern, url);
            if (!StringUtils.isEmpty(insId)) {
//                startFetchUrl(Arrays.asList(url));
            }
        }
        result.setSuccess(false);
        result.setErrorCode(ErrorMsg.ERROR_UNKNOWN_URL);
        result.setErrorMsg("unknown url, cant not fetch user id or note id, please check input url.");
        return result;
    }

    public void downloadUrlToFiles(DownloadItem item, String fileSuffix, boolean force) {
        int i = 1;
        List<String> picUrls = item.getDownloadUrls();
        for (String picUrl : picUrls) {
            if (!StringUtils.isEmpty(picUrl)) {
                //TODO: need flexible, get suffix?
                String targetFile = getTargetFileFolder(item) + i + fileSuffix;

                List<String> picFiles = item.getDownloadFiles();
                picFiles.add(targetFile);

                File folder = new File(targetFile).getParentFile();
                if (!folder.exists()) {
                    folder.mkdirs();
                }

                File file = new File(targetFile);

                if (force) {
                    if (file.exists()) {
                        file.delete();
                    }
                }

                if (!file.exists()) {
                    TargetPathHelper.saveDownloadFile(picUrl, targetFile);
                } else {
                    logger.info("file already exist: " + targetFile);
                }
                i++;
            }
        }
    }

    public void saveDownloadItem(DownloadItem item) {
        if (item == null || StringUtils.isEmpty(item.getItemId()) || item.getType() == null) {
            throw new IllegalArgumentException("invalid parameters item id empty");
        }

        if (item.getType() == null) {
            throw new IllegalArgumentException("invalid parameters item type empty");
        }

        String itemId = item.getItemId();
        int type = item.getType().getValue();

        DownloadItemEntity entity = dao.findByTypeAndItemId(type, itemId);

        if (entity == null) {
            entity = new DownloadItemEntity();
        }

        entity.setItemId(itemId);
        entity.setType(type);

        entity.setContent(item.getContent());
        entity.setTitle(item.getTitle());
        entity.setDownloadUrls(item.getDownloadUrls());
        entity.setDownloadFiles(item.getDownloadFiles());

        dao.save(entity);

        //save file
        String targetFilePath = getTargetFile(entity);
        TargetPathHelper.saveMetaInfo(entity, targetFilePath);
    }


    private String getTargetFileFolder(DownloadItemEntity entity) {
        return mConf.getRootPath() + File.separator + entity.getType() + File.separator + entity.getItemId()
                + File.separator;
    }

    private String getTargetFileFolder(DownloadItem item) {
        return mConf.getRootPath() + File.separator + item.getType().getValue() + File.separator + item.getItemId()
                + File.separator;
    }

    private String getTargetFile(DownloadItemEntity entity) {
        return getTargetFileFolder(entity )+ "item.txt";
    }


    //export
    @Autowired
    private CommonService commonService;

    public void downloadPinterest(HttpServletRequest request, HttpServletResponse response, String noteId) {
        DownloadItemEntity entity = dao.findByTypeAndItemId(DownloadType.PINTEREST.getValue(), noteId);
        if (entity == null){
            logger.info("download item id not in db., item id:" + noteId);
            return;
        }
        //target folder
        String targetFilePath = getTargetFileFolder(entity);
        commonService.downloadFolderAsZipFile(request, response,noteId, targetFilePath);
    }

    public void downloadByUrl(HttpServletRequest request, HttpServletResponse response, String url) {
        Pattern pattern = Pattern.compile(mConf.getPinterestUrlPattern());

        String downloadItemId = RegexUtil.extract(pattern, url);
        if (!StringUtils.isEmpty(downloadItemId)) {
            downloadPinterest(request, response, downloadItemId);
            return;
        }
    }
}
