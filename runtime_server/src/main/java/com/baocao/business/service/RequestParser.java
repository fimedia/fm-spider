package com.baocao.business.service;

import com.baocao.business.protocol.base.BaseJsonRequest;
import com.baocao.server.base.util.Base64;
import com.baocao.server.base.util.Base64DecoderException;
import com.baocao.server.base.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;

@Service
public class RequestParser {
    private static final Logger logger = LoggerFactory.getLogger(RequestParser.class);

    public<T> T getData(BaseJsonRequest request,Class<T> tClass) {

        if (request.getBody() == null || request.getBody().getData() == null) {
            return null;
        }
        String data = request.getBody().getData();
        byte[] dataBytes = decodeBase64(data);
        if (dataBytes == null) {
            logger.warn("return empty bytes in decodeBase64");
            return null;
        }

        byte[] decryptBytes = decrypt(dataBytes);
        if (decryptBytes == null) {
            logger.warn("return empty bytes in decrypt");
            return null;
        }

        String jsonString = "";

        try {
            jsonString = new String(decryptBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("eroor bytes to strig", e);
        }

        T resut = JsonUtil.toObject(jsonString, tClass);
        return resut;
    }

    private byte[] decodeBase64(String data) {
        //base 64 decode
        byte[] dataBytes = null;
        try {
            dataBytes = Base64.decode(data.getBytes("UTF-8"));
        } catch (Base64DecoderException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return dataBytes;
    }

    private byte[] decrypt(byte[] data) {
        return data;
    }
}
