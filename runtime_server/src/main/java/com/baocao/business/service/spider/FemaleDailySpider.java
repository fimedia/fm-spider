package com.baocao.business.service.spider;

import com.baocao.business.conf.SpiderConf;
import com.baocao.business.model.spider.FemaleDailyProductEntity;
import com.baocao.business.model.spider.FemaleDailyProductEntityDAO;
import com.baocao.business.service.spider.selenium.SeleniumUtil;
import com.baocao.server.base.util.RegexUtil;
import org.apache.commons.lang3.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Selectable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class FemaleDailySpider extends AbstracePageProcessor {
    private Logger logger = LoggerFactory.getLogger(FemaleDailySpider.class);

    public static final String URL_LIST = "https://femaledaily.com/category/";

    //https://reviews.femaledaily.com/products/cheek/contour?brand=&order=popular&page=1
    //https://reviews.femaledaily.com/lips/lip-palette?brand=&order=popular&page=1
    public static final String URL_PAGING = "https://reviews\\.femaledaily\\.com/.+&page=\\d+";

    //https://reviews.femaledaily.com/products/styling-tool/hair-straightener/amara/professional-babyliss-hair-2-in-1-straightner
    public static final String URL_REVIEW_DETAIL = "https://reviews\\.femaledaily\\.com/products/[^/]+/[^/]+/.+";

    @Autowired
    private FemaleDailyProductEntityDAO productDAO;

    Pattern pricePattern = Pattern.compile("(USD\\s*[\\d\\.]+|Rp\\.\\s*[\\d\\.]+)");

    Pattern reviewPattern = Pattern.compile("\\>(\\d+)");

    Pattern categoryPattern = Pattern.compile(">([^<]+)</a>");

    Pattern reviewPagingPattern = Pattern.compile(URL_PAGING);

    Pattern reviewDetailPattern = Pattern.compile(URL_REVIEW_DETAIL);

    private static final int maxPageNum = 50;

    @Autowired
    private SpiderConf mConf;

    @Autowired
    private SeleniumUtil seleniumUtil;

    private List<String> getAllLinks(String url) {

        WebDriver driver = seleniumUtil.getUrl(url, "//div[contains(@class, 'category-landing-list')]", true);
        List<WebElement> elements = driver.findElements(By.xpath("//a"));

        List<String> result = new ArrayList<>();
        for (WebElement webElement : elements) {
            String str = webElement.getAttribute("href");
            result.add(str);
        }

        seleniumUtil.quit(driver);
        return result;
    }

    @Override
    public void process(Page page) {
        String currentUrl = page.getUrl().toString();
        if (currentUrl.startsWith(URL_LIST)) {
            //一级分类页面，获取分页
            List<String> links = getAllLinks(currentUrl);
            List<String> urlList = new ArrayList<>();
            for (String link:links) {
                if (link !=null) {
                    Matcher matcher = reviewPagingPattern.matcher(link);

                    if (matcher.matches()) {
                        urlList.add(link);
                    }
                }
            }

            List<String> targetList = new ArrayList<>();

            for (String url: urlList) {
                for (int i=1;i<=maxPageNum;i++) {
                    String target = url.replace("page=1", "page="+i);
                    logger.info("add paging url: {}", target);

                    targetList.add(target);
                }
            }
            page.addTargetRequests(targetList);
        } else if (reviewDetailPattern.matcher(currentUrl).matches()){
            //详情页分析
            processProductDetailPage(page, currentUrl);
        } else if (reviewPagingPattern.matcher(currentUrl).matches()) {
            //分页页面，获取详情页
            List<String> reviewPages = page.getHtml().links().regex(URL_REVIEW_DETAIL).all();

            page.addTargetRequests(reviewPages);

            for (String review: reviewPages) {
                logger.debug("add detail url: {}", review);
            }
        } else {
            logger.warn("skipped process url: {}", page.getUrl().toString());
        }
    }

    private void processProductDetailPage(Page page, String currentUrl) {
        //review page
        String brand = getTextByIdKey(page, "name-brand");
        String product = getTextByIdKey(page, "name-product");
        String proudctShade = getTextByIdKey(page, "name-product-shade");


//        List<String> contents =
        String price = page.getHtml().xpath("//*[@id=\"top-page\"]//div[contains(@class, 'pd-price')]/text()").toString();
        String priveValue = unescape(price);
        String key = "price";

        prepareKeyValue(page, priveValue, key);

        if (!StringUtils.isEmpty(priveValue)) {
            price = priveValue;
        }

        Selectable ratingSel = page.getHtml().xpath("//*[@id=\"top-page\"]").css(".pd-rate").regex(">([^<]+)", 1);
        String rating = unescape(ratingSel.get());
        prepareKeyValue(page, rating, "rating");

        Selectable reviewSel = page.getHtml().xpath("//*[@id=\"top-page\"]").css(".pd-total");
        List<String> reviewNums = reviewSel.all();
        String review = "";
        if (reviewNums != null) {
            for (String reviewNum : reviewNums) {
                String value = unescape(RegexUtil.extract(reviewPattern, reviewNum));
                prepareKeyValue(page, value, "review");

                if (!StringUtils.isEmpty(value)) {
                    review = value;
                }
            }
        }



        Selectable categorySel = page.getHtml().xpath("//meta[@property=\"category\"]/@content");
//        List<String> categorys =
        String allCategory = categorySel.toString();

        String category1="";
        String category2="";
        String category="";

        if (allCategory != null) {
            // spilt
            String[] categories = allCategory.split(" - ");

            if (categories.length >= 1) {
                category1 = categories[0];
                prepareKeyValue(page, category1, "category1");

            }

            if (categories.length >= 2) {
                category2 = categories[1];
                prepareKeyValue(page, category2, "category2");

            }

            if (categories.length >= 3) {
                category = categories[2];
                prepareKeyValue(page, category, "category");
            }
        }


        if (!StringUtils.isEmpty(product) && !StringUtils.isEmpty(product)) {

            FemaleDailyProductEntity entity = new FemaleDailyProductEntity();
            List<FemaleDailyProductEntity> productListSameUrl = productDAO.findProductByWebPageUrl(currentUrl);
            if (!productListSameUrl.isEmpty()) {
                //skipp this url, already exist
                logger.info("url already exist in db, only update{}", currentUrl);

                entity = productListSameUrl.get(0);
            }

            entity.setBrand(brand);

            StringBuilder displayProduct = new StringBuilder();


            if (!product.startsWith(brand)) {
                displayProduct.append(brand).append(" ");
            }
            displayProduct.append(product);
            if (!StringUtils.isEmpty(proudctShade)) {
                displayProduct.append(" ").append(proudctShade);
            }
            entity.setProduct(displayProduct.toString());
            entity.setPrice(price);
            entity.setRating(rating);

            int reviewNum = 0;
            try {
                reviewNum = Integer.parseInt(review);
            } catch (Exception e) {
                logger.error("error parse the review value:" + review, e);
                reviewNum = 0;
            }
            entity.setReview(reviewNum);
            entity.setCategory(category);
            entity.setCategory1(category1);
            entity.setCategory2(category2);
            entity.setWebPageUrl(currentUrl);

            productDAO.save(entity);
        } else {
            logger.error("can not fetch brand and product for url:{}", currentUrl);
        }
    }

    private String unescape(String value) {
        return StringEscapeUtils.unescapeHtml4(value);
    }

    private void prepareKeyValue(Page page, String price, String key) {
        if (!StringUtils.isEmpty(price)) {
            page.putField(key, price);
            logger.info(key + " {}", price);
        }
    }

    private String getTextByIdKey(Page page, String key) {
        String template = "//*[@id=\"%s\"]/text()";
        String xpath = String.format(template, key);
        Selectable textObj = page.getHtml().xpath(xpath);
        String value = unescape(textObj.get());
        page.putField(key, value);

        logger.info("getValueByIdKey, xpath: {}, key :{}, value : {}", xpath, key, value);
        return value;
    }
}
