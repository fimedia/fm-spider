package com.baocao.business.service.spider.pipeline;

import com.baocao.business.model.spider.NoteEntity;
import com.baocao.business.service.spider.model.Note;
import com.baocao.server.base.util.JsonUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.codecraft.webmagic.ResultItems;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TargetPathHelper {

    private static final Logger logger = LoggerFactory.getLogger(TargetPathHelper.class);

    public static String getTargetFilePath(ResultItems resultItems) {
        Note note = resultItems.get("note");
        String filePath = note.userId + File.separator + note.noteId;
        return filePath;
    }

    public static String getTargetFilePath(NoteEntity note) {
        String filePath = note.getUserId() + File.separator + note.getNoteId();
        return filePath;
    }

    public static String getTargetFilePath(String userId, String noteId) {
        String filePath = userId + File.separator + noteId;
        return filePath;
    }

    public static void saveDownloadFile(String picUrl, final String targetFile) {

        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setSocketTimeout(30000)
                .setConnectTimeout(30000)
                .setConnectionRequestTimeout(30000)
                .build();

        CloseableHttpClient httpclient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        try {
            HttpGet httpget = new HttpGet(picUrl);
            logger.info("Executing request:"  + httpget.getRequestLine());
            // Create a custom response handler
            ResponseHandler<File> responseHandler = new ResponseHandler<File>() {

                @Override
                public File handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        InputStream is = entity.getContent();
                        OutputStream outputStream = new FileOutputStream(targetFile);
                        IOUtils.copy(is, outputStream);
                        outputStream.close();
                        logger.info("file saved. [File]" + targetFile);
                        return new File(targetFile);
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status +", url:"+picUrl);
                    }
                }
            };
            httpclient.execute(httpget, responseHandler);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpclient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void saveMetaInfo(Object entity, String targetFilePath) {
        OutputStream outputStream = null;
        try {
            File targetFile =  new File(targetFilePath);
            File folderFile = targetFile.getParentFile();
            if (!folderFile.exists()) {
                folderFile.mkdirs();
            }
            outputStream = new FileOutputStream(targetFilePath);
            String data = JsonUtil.toString(entity);
            org.apache.commons.io.IOUtils.write(data, outputStream);
            logger.info("meta info saved in {}. content:{}", targetFilePath, data);

        } catch (FileNotFoundException e) {
            logger.error("error while saving meta data", e);
        } catch (IOException e) {
            logger.error("error while saving meta data", e);
        }finally {
            org.apache.commons.io.IOUtils.closeQuietly(outputStream);
        }
    }
}
