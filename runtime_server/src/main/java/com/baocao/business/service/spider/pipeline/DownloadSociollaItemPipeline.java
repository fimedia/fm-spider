package com.baocao.business.service.spider.pipeline;

import com.alibaba.fastjson.JSONObject;
import com.baocao.business.model.spider.DownloadSociollaCommentEntity;
import com.baocao.business.model.spider.DownloadSociollaCommentEntityDAO;
import com.baocao.business.model.spider.DownloadSociollaltemEntity;
import com.baocao.business.model.spider.DownloadSociollaltemEntityDAO;
import com.baocao.business.protocol.sociolla.SociollaProductCommentInfo;
import com.baocao.business.protocol.sociolla.SociollaProductInfo;
import com.baocao.business.protocol.sociolla.SociollaProductRatingInfo;
import com.baocao.server.base.util.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.assertj.core.util.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.baocao.business.model.constant.KafkaConstants.SPIDER_PRODUCT_SUCCESS;

@Component
public class DownloadSociollaItemPipeline implements Pipeline {
    private static final Logger logger = LoggerFactory.getLogger(DownloadSociollaItemPipeline.class);

    private static final String SOCIOLLA_PRODUCT_STORAGE = "sociolla_product_storage";

    private static final String SOCIOLLA_PRODUCT_PRICE_UPDATE = "sociolla_product_price_update";

    private static final String SOCIOLLA_PPRODUCT_COMMENTS = "sociolla_product_comments";

    private String source = "sociolla.com";

    private String origId = "sociolla_%s";


    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Autowired
    private DownloadSociollaltemEntityDAO downloadSociollaltemEntityDAO;

    @Autowired
    private DownloadSociollaCommentEntityDAO downloadSociollaCommentEntityDAO;

    @Override
    public void process(ResultItems resultItems, Task task) {
        Map<String, Object> all = resultItems.getAll();
        if (MapUtils.isEmpty(all)) {
            return;
        }
        String type = (String) all.get("type");
        String producId = (String) all.get("product_id");
        if ("product_infno".equalsIgnoreCase(type)) {
            DownloadSociollaltemEntity entity = downloadSociollaltemEntityDAO.findByOrigId(String.format(origId, producId));
            SociollaProductInfo newSociollaProductInfo = (SociollaProductInfo) all.get("product_info");
            //新增商品信息
            if (entity == null && newSociollaProductInfo != null) {
                entity = new DownloadSociollaltemEntity();
                entity.setStatus(0);
                entity.setCreateTime(new Date());
                entity.setOrigId(String.format(origId, producId));
                entity.setOrigUrl((String) all.get("current_url"));
                entity.setSource(source);
                entity.setProductInfo(JSONObject.toJSONString(newSociollaProductInfo));
                entity.setUpdateTime(new Date());
                downloadSociollaltemEntityDAO.save(entity);
                return;
            } else if (newSociollaProductInfo != null) {
                //更新商品价格
                SociollaProductInfo sociollaProductInfo = JSONObject.parseObject(entity.getProductInfo(), SociollaProductInfo.class);
                Map<String, Map<String, Object>> newSpecifications = new HashMap<>(20);
                newSociollaProductInfo.getSpecifications().forEach(stringObjectMap -> {
                    String attributesId = (String) stringObjectMap.get("attributesId");
                    newSpecifications.put(attributesId, stringObjectMap);

                });
                boolean isUpdate = false;
                if (CollectionUtils.isNotEmpty(newSociollaProductInfo.getSpecifications())) {
                    List<Map<String, Object>> mapList = Lists.newArrayList();
                    for (Map<String, Object> stringObjectMap : sociollaProductInfo.getSpecifications()) {
                        String attributesId = (String) stringObjectMap.get("attributesId");
                        Map<String, Object> newMap = newSpecifications.get(attributesId);
                        if (newMap != null) {
                            BigDecimal newReduction = (BigDecimal) newMap.get("reduction");
                            BigDecimal newOrigPrice = (BigDecimal) newMap.get("orig_price");
                            Integer lastReduction = (Integer) stringObjectMap.get("reduction");
                            Integer lastOrigPrice = (Integer) stringObjectMap.get("orig_price");
                            if (newReduction.compareTo(new BigDecimal(lastReduction)) == 0 && newOrigPrice.compareTo(new BigDecimal(lastOrigPrice)) == 0) {
                                continue;
                            }
                            stringObjectMap.put("orig_price", newOrigPrice);
                            stringObjectMap.put("reduction", newReduction);
                            isUpdate = true;
                        }
                        mapList.add(stringObjectMap);
                    }
                    sociollaProductInfo.setSpecifications(mapList);
                }
                if (isUpdate) {
                    entity.setProductInfo(JSONObject.toJSONString(sociollaProductInfo));
                    entity.setUpdateTime(new Date());
                    downloadSociollaltemEntityDAO.save(entity);
                    sendMessage(entity.getSource(), entity.getOrigId(), null, SOCIOLLA_PRODUCT_PRICE_UPDATE);
                }
            }
            return;
        }
        if ("product_image".equalsIgnoreCase(type)) {
            String productAttributeId = resultItems.get("id_product_attribute");
            List<String> imageList = resultItems.get("image_list");
            saveImage(producId, productAttributeId, imageList);
        }
        if ("product_rating".equalsIgnoreCase(type)) {
            DownloadSociollaltemEntity entity = downloadSociollaltemEntityDAO.findByOrigId(String.format(origId, producId));
            if (entity == null) {
                return;
            }
            SociollaProductInfo sociollaProductInfo = JSONObject.parseObject(entity.getProductInfo(), SociollaProductInfo.class);
            SociollaProductRatingInfo sociollaProductRatingInfo = (SociollaProductRatingInfo) all.get("rating_info");
            if (sociollaProductRatingInfo == null) {
                return;
            }
            sociollaProductInfo.setProductRatingInfo(sociollaProductRatingInfo);
            entity.setProductInfo(JSONObject.toJSONString(sociollaProductInfo));
            int productStatus = entity.getStatus();
            if (checkProductInfoSucess(sociollaProductInfo)) {
                entity.setStatus(1);
            }
            entity.setUpdateTime(new Date());
            if (productStatus == 0) {
                downloadSociollaltemEntityDAO.save(entity);
                if (entity.getStatus() == 1) {
                    sendMessage(entity.getSource(), entity.getOrigId(), null , SOCIOLLA_PRODUCT_STORAGE);
                }
            }
        }
        if ("product_comment".equalsIgnoreCase(type)) {
            List<SociollaProductCommentInfo> sociollaProductCommentInfoList = (List<SociollaProductCommentInfo>) all.get("product_comment");
            if (CollectionUtils.isEmpty(sociollaProductCommentInfoList)) {
                return;
            }
            String currentUrl = (String) all.get("current_url");
            String origId = String.format(this.origId, producId);
            List<String> commentIds = new ArrayList<>(sociollaProductCommentInfoList.size());
            List<DownloadSociollaCommentEntity> commentEntityList = new ArrayList<>(sociollaProductCommentInfoList.size());
            sociollaProductCommentInfoList.forEach(sociollaProductCommentInfo -> {
                DownloadSociollaCommentEntity commentEntity = downloadSociollaCommentEntityDAO.findByCommentIdAndProductId(sociollaProductCommentInfo.getCommentId(), origId);
                if (!Objects.nonNull(commentEntity)) {
                    commentEntity = new DownloadSociollaCommentEntity();
                    commentEntity.setCommentId(sociollaProductCommentInfo.getCommentId());
                    commentEntity.setComment(sociollaProductCommentInfo.getComment());
                    commentEntity.setProductId(origId);
                    commentEntity.setCreateTime(new Date());
                    commentEntity.setRantingInfo(JSONObject.toJSONString(sociollaProductCommentInfo.getRantingInfo()));
                    commentEntity.setSource(source);
                    commentEntity.setOrigUrl(currentUrl);
                    commentIds.add(commentEntity.getCommentId());
                    commentEntityList.add(commentEntity);
                }
            });
            if(CollectionUtils.isNotEmpty(commentEntityList)){
                downloadSociollaCommentEntityDAO.saveAll(commentEntityList);
                sendMessage(source, origId, commentIds,  SOCIOLLA_PPRODUCT_COMMENTS);
            }

        }
    }

    /**
     * kafka消息通知
     *
     * @param source
     * @param origId
     * @param event
     */
    public void sendMessage(String source, String origId, List<String> commentIds, String event) {
        Map<String, Object> kafkaMessage = new HashMap<>();
        kafkaMessage.put("source", source);
        kafkaMessage.put("event", event);
        Map<String, Object> dataMessage = new HashMap<>();
        dataMessage.put("id", origId);
        if(CollectionUtils.isNotEmpty(commentIds)){
            dataMessage.put("comment_ids", commentIds);
        }
        dataMessage.put("tm", System.currentTimeMillis());
        kafkaMessage.put("data", dataMessage);
        kafkaTemplate.send(SPIDER_PRODUCT_SUCCESS, JSONObject.toJSONString(kafkaMessage));
        logger.info("发送Kafka消息成功！{}", JSONObject.toJSONString(kafkaMessage));
    }

    /**
     * 检查爬虫爬取的数据是否完整
     *
     * @param sociollaProductInfo
     * @return
     */
    private boolean checkProductInfoSucess(SociollaProductInfo sociollaProductInfo) {
        SociollaProductRatingInfo productRatingInfo = sociollaProductInfo.getProductRatingInfo();
        if (productRatingInfo == null) {
            return false;
        }
        List<Map<String, Object>> specifications = sociollaProductInfo.getSpecifications();
        if (CollectionUtils.isEmpty(specifications)) {
            return false;
        }
        for (Map<String, Object> stringObjectMap : sociollaProductInfo.getSpecifications()) {
            List<String> images = (List<String>) stringObjectMap.get("images");
            if (CollectionUtils.isEmpty(images)) {
                return false;
            }
        }
        return true;
    }


    private synchronized void saveImage(String producId, String productAttributeId, List<String> imageList) {
        DownloadSociollaltemEntity entity = downloadSociollaltemEntityDAO.findByOrigId(String.format(origId, producId));
        if (entity != null) {
            SociollaProductInfo sociollaProductInfo = JSONObject.parseObject(entity.getProductInfo(), SociollaProductInfo.class);
            List<Map<String, Object>> specifications = sociollaProductInfo.getSpecifications();
            if (CollectionUtils.isEmpty(specifications)) {
                return;
            }
            specifications.forEach(stringObjectMap -> {
                String attributesId = (String) stringObjectMap.get("combination_id");
                if (productAttributeId != null && productAttributeId.equalsIgnoreCase(attributesId)) {
                    stringObjectMap.put("images", imageList);
                }
            });
            entity.setProductInfo(JSONObject.toJSONString(sociollaProductInfo));
            int productStatus = entity.getStatus();
            if (checkProductInfoSucess(sociollaProductInfo)) {
                entity.setStatus(1);
            }
            entity.setUpdateTime(new Date());
            if (productStatus == 0) {
                downloadSociollaltemEntityDAO.save(entity);
                if (entity.getStatus() == 1) {
                    sendMessage(entity.getSource(), entity.getOrigId(), null, SOCIOLLA_PRODUCT_STORAGE);
                }
            }

        }
    }
}
