package com.baocao.business.service.spider.model;

import java.util.List;

public class Note {

    public String title;
    public String content;
    public List<String> picUrls;
    public List<String> videoUrls;

    public String like;
    public String comment;
    public String star;

    public String noteId;
    public String webPageUrl;

    public String userId;

    @Override
    public String toString() {
        return "Note{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", picUrls=" + picUrls + '\'' +
                ", videoUrls= " + videoUrls +'\''+
                ", like='" + like + '\'' +
                ", comment='" + comment + '\'' +
                ", star='" + star + '\'' +
                ", noteId='" + noteId + '\'' +
                ", webPageUrl='" + webPageUrl + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
