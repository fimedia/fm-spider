package com.baocao.business.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baocao.business.conf.SpiderConf;
import com.baocao.business.model.spider.*;
import com.baocao.business.protocol.note.FetchResult;
import com.baocao.business.service.spider.NoteSpider;
import com.baocao.business.service.spider.model.Note;
import com.baocao.business.service.spider.pipeline.NoteSavePipeline;
import com.baocao.business.service.spider.pipeline.PicSavePipeline;
import com.baocao.business.service.spider.pipeline.TargetPathHelper;
import com.baocao.server.base.compress.IOUtils;
import com.baocao.server.base.util.RegexUtil;
import com.vox.xhs.ServerException;
import com.vox.xhs.XHSApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.zip.ZipOutputStream;

@Component
public class NoteService {

    @Autowired
    private SpiderConf mConf;

    private Logger logger = LoggerFactory.getLogger(NoteService.class);

    public static final int ERROR_UNKNOWN_URL = ErrorMsg.ERROR_UNKNOWN_URL;//unknown url.

    public static final int ERROR_CODE_EXIST = ErrorMsg.ERROR_CODE_EXIST;//Already exist.
    public static final int ERROR_CODE = ErrorMsg.ERROR_CODE;//exception

    @Autowired
    private NoteSpider noteSpiter;

    Pattern noteUrlPattern;

    Pattern userUrlPattern;

    private synchronized Spider createSpider() {
        noteSpiter.noteUrlPattern = getNoteUrlPattern();
        noteSpiter.userUrlPattern = getUserUrlPattern();

        Spider spiderInstance = Spider.create(noteSpiter)
                .addPipeline(mPicSavePipeline)
                .addPipeline(mNoteSavePipeline)
                .setExitWhenComplete(true)
                .thread(mConf.getSpiderThreadNumber());
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
        httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy(mConf.getProxyUrl(), 9180, mConf.getProxyUserName(), mConf.getProxyPassword())));
        spiderInstance.setDownloader(httpClientDownloader);

        return spiderInstance;
    }

    private String getRootFilePath() {
        return mConf.getRootPath();
    }


    private  static XHSApi apiClient;

    public NoteService() {
        try {
            apiClient =  new XHSApi();
        } catch (IOException e) {
            logger.error("init api client io error", e);
        } catch (ServerException e) {
            logger.error("init api client server error", e);
        } catch (Throwable e) {
            logger.error("init api client unknown error", e);
        }
    }

    @Autowired
    private NoteSavePipeline mNoteSavePipeline;

    @Autowired
    private PicSavePipeline mPicSavePipeline;

    @Autowired
    private NoteUserEntityDAO noteUserEntityDAO;

    @Autowired
    private NoteEntityDAO noteEntityDAO;

    @Transactional(propagation = Propagation.REQUIRED)
    public FetchResult fetchNoteByNoteIdAPI(String noteId, boolean force) {
        FetchResult result = new FetchResult();

        if (!force) {
            NoteEntity current = noteEntityDAO.findNoteEntityByNoteId(noteId);
            if (current!=null) {
                result.setSuccess(false);
                result.setErrorCode(ERROR_CODE_EXIST);
                result.setOperation("already exist this note id:" + noteId);
                return result;
            }
        }

        //
        try {
            JSONArray array = apiClient.noteFeed(noteId);

            if (array!=null && array.size() > 0){
                JSONObject jsonObject = array.getJSONObject(0);
                if(jsonObject.containsKey("note_list")) {
                    JSONObject note1 = jsonObject.getJSONArray("note_list").getJSONObject(0);
                    Note note = new Note();

                    note.noteId = noteId;

                    note.userId = "";
                    // 标题
                    note.title = note1.getString("title");
                    // 正文
                    note.content = note1.getString("desc");
                    // 图片
                    List<String> picUrls = new ArrayList<>();
                    note.picUrls = picUrls;
                    if (note1.containsKey("images_list")) {
                        JSONArray imageList = note1.getJSONArray("images_list");
                        for (int i = 0; i < imageList.size(); i++) {
                            JSONObject image = imageList.getJSONObject(i);
                            // 图片URL
                            picUrls.add(image.getString("url"));
                        }
                    }

                    if (note1.containsKey("user")) {
                        JSONObject user = note1.getJSONObject("user");
                        note.userId = user.getString("id");
                    }

                    if (note1.containsKey("mini_program_info")) {
                        JSONObject user = note1.getJSONObject("mini_program_info");
                        note.webPageUrl = user.getString("webpage_url");
                    }
                    // 收藏数
                    note.star = note1.getString("collected_count");
                    // 评论数
                    note.comment = note1.getString("comments_count");
                    // 点赞数
                    note.like = note1.getString("liked_count");

                    List<String> videoUrls = new ArrayList<>();
                    note.videoUrls = videoUrls;
                    if (note1.getString("type").equals("video")) {
                        if (note1.containsKey("video")) {
                            //video url
                            String url = note1.getJSONObject("video").getString("url");
                            videoUrls.add(url);
                        }
                    }

                    List<String> picFiles = new ArrayList<>();
                    downloadUrlToFiles(note, note.picUrls, picFiles, ".jpg", force);

                    List<String> videoFiles = new ArrayList<>();
                    downloadUrlToFiles(note, note.videoUrls, videoFiles, ".mp4", force);

                    saveNote(note, picFiles, videoFiles);
                }
            }
        } catch (IOException e) {
            result.setErrorCode(ERROR_CODE);
            result.setErrorMsg(e.getMessage());
            logger.error("error while fetch note by api.", e);
        } catch (ServerException e) {
            result.setErrorCode(ERROR_CODE);
            result.setErrorMsg(e.getMessage());
            logger.error("error while fetch note by api.", e);
        }
        return result;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveNote(Note note, List<String> picFiles, List<String> videoFiles) {
        NoteEntity entity = null;
        if (!StringUtils.isEmpty(note.noteId)) {
            entity = noteEntityDAO.findNoteEntityByNoteId(note.noteId);
        }

        if (entity!=null) {
            //update
        }else {
            entity = new NoteEntity();
        }

        entity.setTitle(note.title);
        entity.setContent(note.content);
        entity.setPicUrls(note.picUrls);
        entity.setVideoUrls(note.videoUrls);
        entity.setLikeCount(note.like);
        entity.setStar(note.star);
        entity.setComment(note.comment);
        entity.setNoteId(note.noteId);
        entity.setUserId(note.userId);
        entity.setWebPageUrl(note.webPageUrl);

        if (picFiles!=null) {
            if (!picFiles.isEmpty()) {
                entity.setPicFiles(picFiles);
            }
        }

        if (videoFiles!=null) {
            if (!videoFiles.isEmpty()) {
                entity.setVidoeFiles(videoFiles);
            }
        }

        entity = noteEntityDAO.save(entity);
        logger.info("note entity saved. id:{}", entity.getId());

        //save file
        String targetFilePath = getTargetFilePath(entity) + File.separator + "note.txt";
        TargetPathHelper.saveMetaInfo(entity, targetFilePath);
    }

    public void downloadUrlToFiles(Note note, List<String> picUrls, List<String> picFiles, String fileSuffix, boolean force) {
        int i = 1;
        for (String picUrl : picUrls) {
            if (!StringUtils.isEmpty(picUrl)) {
                String targetFile = getTargetFilePath(i, picUrl, fileSuffix,note.userId ,note.noteId );
                picFiles.add(targetFile);

                File folder = new File(targetFile).getParentFile();
                if (!folder.exists()) {
                    folder.mkdirs();
                }

                File file = new File(targetFile);

                if (force) {
                    if (file.exists()) {
                        file.delete();
                    }
                }

                if (!file.exists()) {
                    TargetPathHelper.saveDownloadFile(picUrl, targetFile);
                    logger.info("file saved in : " + targetFile);
                } else {
                  logger.info("file already exist: " + targetFile);
                }
                i++;
            }
        }
    }

    private String getTargetFilePath(String userId, String noteId) {
        return getRootFilePath() + File.separator + TargetPathHelper.getTargetFilePath(userId, noteId);
    }

    private Pattern mPicUrlPattern = Pattern.compile("http://ci.xiaohongshu.com/([^?]+)");

    protected String getTargetFilePath(int i, String picUrl, String fileSuffix, String userId, String noteId) {
        String targetFilePath = getTargetFilePath(userId, noteId);

        String fileName = i + "_";
        fileName += RegexUtil.extract(mPicUrlPattern, picUrl);
        if (StringUtils.isEmpty(fileName)) {
            fileName = String.valueOf(Calendar.getInstance().getTimeInMillis());
        }
        fileName += fileSuffix;
        return targetFilePath + File.separator + fileName;
    }

    private String getTargetFilePath(NoteEntity note) {
        return getRootFilePath() + File.separator + TargetPathHelper.getTargetFilePath(note);
    }

    public FetchResult fetchNoteByNoteId(String noteId, boolean force) {
        FetchResult result = new FetchResult();

        if (!force) {
            NoteEntity current = noteEntityDAO.findNoteEntityByNoteId(noteId);
            if (current!=null) {
                result.setSuccess(false);
                result.setErrorCode(ERROR_CODE_EXIST);
                result.setOperation("already exist this note id:" + noteId);
                return result;
            }
        }

        List<String> startUrls = Arrays.asList(mConf.getNotePrefix() + noteId);
        startFetchNoteUrl(startUrls);
        result.setSuccess(true);
        return result;
    }

    private void startFetchNoteUrl(List<String> startUrls) {
        Spider spider = createSpider();
//        spider.addUrl(startUrls.toArray(new String[startUrls.size()]));
//        if (spider.getStatus() == Spider.Status.Init || spider.getStatus() == Spider.Status.Stopped) {
//            spider.start();
//        }
        List<ResultItems> resultItems = spider.getAll(startUrls);
        logger.info("spider get all finished {}", resultItems == null ? 0 : resultItems.size());
    }

    public FetchResult fetchUserNotesNyUrl(JobTypeEnum jobType, String url, boolean force) {
        String noteId = RegexUtil.extract(getNoteUrlPattern(), url);
        if (!StringUtils.isEmpty(noteId)) {
            if (jobType == JobTypeEnum.API_FETCH) {
                return fetchNoteByNoteIdAPI(noteId, force);
            }else {
                return fetchNoteByNoteId(noteId, force);
            }
        }

        String userId = RegexUtil.extract(getUserUrlPattern(), url);
        if (!StringUtils.isEmpty(userId)) {
            return fetchUserNotes(userId, force);
        }

        FetchResult result = new FetchResult();
        result.setSuccess(false);
        result.setErrorCode(ERROR_UNKNOWN_URL);
        result.setErrorMsg("unknown url, cant not fetch user id or note id, please check input url.");
        return result;
    }

    private Pattern getUserUrlPattern() {
        if (userUrlPattern == null) {
            userUrlPattern = Pattern.compile(mConf.getUserUrlPattern());
        }
        return userUrlPattern;
    }

    private Pattern getNoteUrlPattern() {
        if (noteUrlPattern == null) {
            noteUrlPattern = Pattern.compile(mConf.getNoteUrlPattern());
        }
        return noteUrlPattern;
    }

    public void downloadNotesByUrl(HttpServletRequest request, HttpServletResponse response, String url) {
        String noteId = RegexUtil.extract(getNoteUrlPattern(), url);
        if (!StringUtils.isEmpty(noteId)) {
            downloadNote(request, response, noteId);
            return;
        }

        String userId = RegexUtil.extract(getUserUrlPattern(), url);
        if (!StringUtils.isEmpty(userId)) {
            downloadUser(request, response, userId);
            return;
        }
    }

    public FetchResult fetchUserNotes(String userId, boolean force) {
        FetchResult result = new FetchResult();

        boolean videoOnly = false;
        int page = 0;
        int pageSize = 10;
        JSONObject jsonObject = null;
        try {
            jsonObject = apiClient.noteUser(userId, videoOnly, page, pageSize);
        } catch (Exception e) {
            clientErrorHandling(result, getStackMsg(e));
        }

        if (jsonObject == null) {
            return result;
        }

        int totalCount = jsonObject.getInteger("total_notes_count");

        //create user and user id
        NoteUserEntity userEntity = null;
        JSONArray noteArray = jsonObject.getJSONArray("notes");
        int noteCount = noteArray.size();

        List<String> noteUrlList = new ArrayList<>();

        boolean needUpdate = false;
        for (int i=0;i<noteCount;i++) {
            JSONObject noteObject = noteArray.getJSONObject(i);

            JSONObject userObj = noteObject.getJSONObject("user");
            String userid = userObj.getString("userid");

            if (!StringUtils.isEmpty(userId)) {
                userEntity = noteUserEntityDAO.findUserByUserId(userId);

                if (userEntity == null) {
                    //create
                    userEntity = new NoteUserEntity();
                }

                //is already exist user and no update notes
                if (!needUpdate && userEntity.getTotalCount() == totalCount && !force) {
                    result.setSuccess(false);
                    result.setErrorCode(ERROR_CODE_EXIST);
                    result.setOperation("already exist this user id:" + userId + ", no update notes, total count:" + totalCount);
                    return result;
                } else {
                    needUpdate = true;
                }
                if (i==0) {
                    userEntity.setNickName(userObj.getString("nickname"));
                    userEntity.setUserId(userid);
                    userEntity.setImageUrl(userObj.getString("images"));
                    userEntity.setTotalCount(totalCount);
                }
                //save
                userEntity = noteUserEntityDAO.save(userEntity);
            }

            //add url to spider to download note, and associate with the user id
            JSONObject infoObj = noteObject.getJSONObject("mini_program_info");
            String webpageUrl = infoObj.getString("webpage_url");

            if (!StringUtils.isEmpty(webpageUrl)) {
                noteUrlList.add(webpageUrl);
            }
        }

        if (!noteUrlList.isEmpty()) {
            startFetchNoteUrl(noteUrlList);
        }

        //fetch the other

        int fetchCount = (page+1) * pageSize;
        while (fetchCount < totalCount) {
            //next page
            page = page + 1;

            jsonObject = null;
            try {
                jsonObject = apiClient.noteUser(userId, videoOnly, page, pageSize);
            } catch (Exception e) {
                clientErrorHandling(result, e.getMessage());
            }

            if (jsonObject == null) {
                return result;
            }

            noteArray = jsonObject.getJSONArray("notes");
            noteCount = noteArray.size();

            noteUrlList = new ArrayList<>();
            for (int i=0;i<noteCount;i++) {
                JSONObject noteObject = noteArray.getJSONObject(i);
                JSONObject infoObj = noteObject.getJSONObject("mini_program_info");
                String webpageUrl = infoObj.getString("webpage_url");

                if (!StringUtils.isEmpty(webpageUrl)) {
                    noteUrlList.add(webpageUrl);
                }
            }
            if (!noteUrlList.isEmpty()) {
                startFetchNoteUrl(noteUrlList);
            }

            fetchCount = (page+1) * pageSize;
        }
        result.setSuccess(true);
        return result;
    }

    private void clientErrorHandling(FetchResult result, String message) {
        userActivative();
        result.setSuccess(false);
        result.setErrorCode(ERROR_CODE);
        result.setErrorMsg(message);
    }

    private void userActivative() {
        try {
            apiClient.userActivate();
        } catch (IOException e1) {
            logger.warn("user activate, for note user.", e1);
        } catch (ServerException e1) {
            e1.printStackTrace();
            logger.warn("user activate, for note user.", e1);
        }
    }

    public List<NoteEntity> listNotes() {
        return noteEntityDAO.findAll();
    }

    public List<NoteUserEntity> listUsers() {
        return noteUserEntityDAO.findAll();
    }

    public List<NoteEntity> findNoteByUserId(String userId) {
        return noteEntityDAO.findNoteEntityByUserId(userId);
    }

    public NoteEntity findNoteByNoteId(String noteId) {
        return noteEntityDAO.findNoteEntityByNoteId(noteId);
    }

    private static String getStackMsg(Throwable e) {

        StringBuffer sb = new StringBuffer();
        StackTraceElement[] stackArray = e.getStackTrace();
        for (int i = 0; i < stackArray.length; i++) {
            StackTraceElement element = stackArray[i];
            sb.append(element.toString() + "\n");
        }
        return sb.toString();
    }

    public void downloadUser(HttpServletRequest request, HttpServletResponse response, String userId) {
        NoteUserEntity entity = noteUserEntityDAO.findUserByUserId(userId);
        if (entity == null){
            logger.info("user id not in db., user id:" + userId);
            return;
        }
        //target folder
        String targetFilePath = getRootFilePath() + File.separator + entity.getUserId();
        downloadFolderAsZipFile(request, response,userId, targetFilePath);
    }

    public void downloadNote(HttpServletRequest request, HttpServletResponse response, String noteId) {
        NoteEntity entity = noteEntityDAO.findNoteEntityByNoteId(noteId);
        if (entity == null){
            logger.info("note id not in db., note id:" + noteId);
            return;
        }
        //target folder
        String targetFilePath = getRootFilePath() + File.separator + entity.getUserId() + File.separator + noteId;
        downloadFolderAsZipFile(request, response,noteId, targetFilePath);
    }

    private void downloadFolderAsZipFile(HttpServletRequest request, HttpServletResponse response,String zipNamePrefix, String targetFilePath) {
        try {
            String zipName = zipNamePrefix + "_" + UUID.randomUUID().toString()+".zip";
            String outFilePath = request.getSession().getServletContext().getRealPath("/");
            File fileZip = new File(outFilePath + "/" +zipName);

            ArrayList<File> inputFiles = IOUtils.getFiles(targetFilePath);
            FileOutputStream outStream = new FileOutputStream(fileZip);
            ZipOutputStream toClient = new ZipOutputStream(outStream);
            IOUtils.zipFile(getRootFilePath(), inputFiles,toClient);
            toClient.close();
            outStream.close();
            IOUtils.downloadFile(fileZip,response,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
