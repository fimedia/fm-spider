package com.baocao.business.service.spider;

import com.baocao.business.conf.SpiderConf;
import com.baocao.business.service.spider.model.DownloadItem;
import com.baocao.server.base.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;
import java.util.regex.Pattern;

@Component
public class InstagramSpider extends AbstracePageProcessor {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Pattern metaPattern = Pattern.compile("<meta property=\"og:video\" content=\"([^\"]+)\">");

    @Autowired
    private SpiderConf conf;

    @Override
    public void process(Page page) {
        //video page?
        ///html/head/meta[24]
        Selectable metaSelector = page.getHtml().xpath("html/head/meta");

        DownloadItem item  = new DownloadItem();
        page.putField("item", item);

        item.setPageUrl(page.getUrl().get());

        Pattern insPattern = Pattern.compile(conf.getInsUrlPattern());
        String itemId = RegexUtil.extract(insPattern, item.getPageUrl());

        logger.info("url: {}, id: {}", item.getPageUrl(), itemId);
        item.setItemId(itemId);

        List<String> values = metaSelector.all();
        if (values!=null) {
            for (String value : values) {
                String downloadUrl = RegexUtil.extract(metaPattern, value);

                if (!StringUtils.isEmpty(downloadUrl)) {
                    logger.info("download url extrac: {}" , downloadUrl);
//                    page.putField("url", downloadUrl);
                    item.getDownloadUrls().add(downloadUrl);
                }
            }
        }
    }
}
