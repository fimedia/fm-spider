package com.baocao.business.service.spider;

import com.baocao.business.conf.SpiderConf;
import com.baocao.business.service.spider.model.DownloadItem;
import com.baocao.business.service.spider.model.DownloadType;
import com.baocao.server.base.util.RegexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Selectable;

import java.util.regex.Pattern;

@Component
public class PinterestSpider extends AbstracePageProcessor {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private Pattern metaPattern = Pattern.compile("<meta property=\"og:video\" content=\"([^\"]+)\">");

    @Autowired
    private SpiderConf conf;

    @Override
    public void process(Page page) {

        DownloadItem item  = new DownloadItem();
        item.setType(DownloadType.PINTEREST);
        page.putField("item", item);

        item.setPageUrl(page.getUrl().get());

        Pattern insPattern = Pattern.compile(conf.getPinterestUrlPattern());
        String itemId = RegexUtil.extract(insPattern, item.getPageUrl());

        logger.info("url: {}, id: {}", item.getPageUrl(), itemId);
        item.setItemId(itemId);

        //video page?
        ///html/head/meta[24]
        Selectable metaSelector = page.getHtml().xpath("//*[@id=\"initial-state\"]");
        String value = metaSelector.get();
        if (value!=null) {
            Pattern mPattern = Pattern.compile("\"url\": \"([^\"]+(?<=\\.mp4))");

            String url = RegexUtil.extract(mPattern, value);
            logger.info("download url extrac: {}" , url);
            item.getDownloadUrls().add(url);
        }


    }
}
