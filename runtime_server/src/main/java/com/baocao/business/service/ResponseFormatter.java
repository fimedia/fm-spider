package com.baocao.business.service;

import com.baocao.business.protocol.base.BaseJsonResponse;
import com.baocao.business.protocol.base.Body;
import com.baocao.business.protocol.base.Header;
import com.baocao.business.protocol.base.data.StateInfo;
import com.baocao.server.base.util.Base64;
import com.baocao.server.base.util.JsonUtil;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;

@Service
public class ResponseFormatter {

    public<T> BaseJsonResponse formatResponse(T data, StateInfo state) {
        Header header = Header.defaultHeader();
        Body body = new Body();


        if (data != null) {
            //to json
            String json = JsonUtil.toString(data);
            //encrypt
            byte[] encryptData = encrypt(json.getBytes(Charset.forName("UTF-8")));
            //base 64
            String dataStr = encodeBase64(encryptData);
            body.setData(dataStr);
        }
        BaseJsonResponse response = new BaseJsonResponse();
        response.setHeader(header);
        response.setBody(body);
        response.setState(state);

        return response;
    }

    private String encodeBase64(byte[] data) {
        //base 64 decode
        String base64Str =  Base64.encode(data);
        return base64Str;
    }

    private byte[] encrypt(byte[] data) {
        return data;
    }
}
