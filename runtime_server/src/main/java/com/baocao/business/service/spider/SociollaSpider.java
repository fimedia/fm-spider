package com.baocao.business.service.spider;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baocao.business.protocol.sociolla.SociollaProductCommentInfo;
import com.baocao.business.protocol.sociolla.SociollaProductInfo;
import com.baocao.business.protocol.sociolla.SociollaProductRatingInfo;
import com.baocao.server.base.util.RegexUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Json;
import us.codecraft.webmagic.selector.Selectable;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Component
public class SociollaSpider extends AbstracePageProcessor {
    private static final Logger logger = LoggerFactory.getLogger(SociollaSpider.class);

    private Pattern productPattern = Pattern.compile("var dataProduct =(.*)};");

    private Pattern combinationsPattern = Pattern.compile("var combinations =(.*)};");

    private Pattern attributesCombinationsPattern = Pattern.compile("var attributesCombinations = (.*)];");


    @Override
    public void process(Page page) {
        String url = page.getUrl().toString();
        if (url.endsWith("/")) {
            List<Selectable> selectableList = page.getHtml().xpath("//div[@class='header-navigation']/div[@id='block_top_menu']/ul[1]/li/ul/li").nodes();
            if (CollectionUtils.isEmpty(selectableList)) {
                logger.warn("未获取到分类列表");
            }
            selectableList.forEach(oneSelectable -> {
                String oneCatgoryName = oneSelectable.xpath("/li/a/text()").get();
                String oneCatgoryUrl = oneSelectable.xpath("/li/a/@href").get();
                List<Selectable> twoSelectableList = oneSelectable.xpath("//div[@class='topblock-parent']").nodes();
                twoSelectableList.forEach(twoSelectable -> {
                    String twoCategroyName = twoSelectable.xpath("/div/a/text()").get();
                    String twoCatgoryUrl = twoSelectable.xpath("/div/a/@href").get();
                    List<Selectable> threeSelectableList = twoSelectable.xpath("//div[@class='topblock-children']/span").nodes();
                    threeSelectableList.forEach(threeSelectable -> {
                        String threeCatgoryName = threeSelectable.xpath("/span/a/text()").get();
                        String threeCatgoryUrl = threeSelectable.xpath("/span/a/@href").get();
                        ArrayList<String> categoryList = Lists.newArrayList(oneCatgoryName, twoCategroyName, threeCatgoryName);
                        addTargetRequestList(page, buildRequestList(threeCatgoryUrl, categoryList, 7L, 1, 1));

                    });
                    ArrayList<String> categoryList = Lists.newArrayList(oneCatgoryName, twoCategroyName);
                    addTargetRequestList(page, buildRequestList(twoCatgoryUrl, categoryList, 6L, 1, 1));
                });
                ArrayList<String> categoryList = Lists.newArrayList(oneCatgoryName);
                addTargetRequestList(page, buildRequestList(oneCatgoryUrl, categoryList, 5L, 1, 1));

            });
        }
        if (!url.endsWith("/")) {
            Html html = page.getHtml();
            List<Selectable> nodes = html.xpath("//div[@class='product-item eec-data']").nodes();
            Map<String, String> productInfoUrlMap = new HashMap<>(20);
            if (CollectionUtils.isNotEmpty(nodes)) {
                nodes.forEach(selectable -> {
                    String productInfoUrl = selectable.xpath("//a[@class='view-detail']/@href").get();
                    String imageUrl = selectable.xpath("//div[@class='pi-img-wrapper']/img[@class='img-responsive']/@src").get();
                    productInfoUrlMap.put(productInfoUrl, imageUrl);
                });
            }
            Map<String, Object> extras = page.getRequest().getExtras();
            String nextPage = html.xpath("//li[@id='pagination_next_bottom']/a/@href").get();
            if (StringUtils.isNotBlank(nextPage)) {
                Request request = new Request();
                request.setExtras(extras);
                request.setPriority(9L);
                request.setUrl("https://www.sociolla.com" + nextPage);
                logger.info("nextPage:{}", nextPage);
                page.addTargetRequest(request);
            }

            buildeRequestList(productInfoUrlMap, 11L).forEach(request -> {
                request.getExtras().putAll(extras);
                page.addTargetRequest(request);
            });
        }
        if (url.endsWith("html")) {
            processProductDetailPage(page, url);
        }
        //图片页面
        if (url.indexOf("loadImageProductAttribute") != -1) {
            Html html = page.getHtml();
            List<String> imageUrlList = html.xpath("//img[@class='img-responsive']/@data-image-small").all();
            if (CollectionUtils.isEmpty(imageUrlList)) {
                logger.info("图片获取失败！url:{}", page.getUrl());
                return;
            }
            Iterator<String> iterator = imageUrlList.iterator();
            while (iterator.hasNext()) {
                if (StringUtils.isBlank(iterator.next())) {
                    iterator.remove();
                }
            }
            logger.info("image:{}", JSONObject.toJSONString(imageUrlList));
            Map<String, String> urlParams = getUrlParams(url);
            page.putField("type", "product_image");
            page.putField("image_list", imageUrlList);
            page.putField("product_id", urlParams.get("id_product"));
            page.putField("id_product_attribute", urlParams.get("id_product_attribute"));

        }
        if (url.indexOf("loadStatisticReview") != -1) {
            SociollaProductRatingInfo sociollaProductRatingInfo = new SociollaProductRatingInfo();
            String dataReview = page.getJson().jsonPath("$.data_review").get();
            if (StringUtils.isBlank(dataReview)) {
                return;
            }
            Document parse = Jsoup.parse(dataReview);
            Element countReviewElement = parse.select("p.count-review").get(0);
            if (countReviewElement != null) {
                Elements elements = countReviewElement.select("strong");
                if (elements == null || elements.size() <= 0) {
                    sociollaProductRatingInfo.setReviewCount(0L);
                } else {
                    String countReview = elements.get(0).text();
                    sociollaProductRatingInfo.setReviewCount(Long.valueOf(countReview));
                }
            }

            String rateValue = parse.select("p.rate-value").get(0).text();
            sociollaProductRatingInfo.setRateValue(Double.valueOf(rateValue));

            //遍历评分
            Elements select = parse.select("div.name-star");
            select.forEach(element -> {
                Element titleNameStar = element.select("span.title-name-star").get(0);
                Element categoryStarCounter = element.select("span.category-star-counter").get(0);
                buildeProductRating(sociollaProductRatingInfo, titleNameStar.text(), categoryStarCounter.text());
            });
            Map<String, String> urlParams = getUrlParams(url);
            page.putField("product_id", urlParams.get("id_product"));
            page.putField("type", "product_rating");
            page.putField("rating_info", sociollaProductRatingInfo);
            logger.info("SociollaProductRatingInfo:{}", JSONObject.toJSONString(sociollaProductRatingInfo));
        }

        if (url.indexOf("api.soco.id/v2/review/product") != -1) {
            Json result = page.getJson();
            String code = result.jsonPath("$.meta.code").get();
            if (StringUtils.isBlank(code) || !"200".equals(code.trim())) {
                return;
            }
            List<String> userCommentList = result.jsonPath("$.data").all();
            if (CollectionUtils.isEmpty(userCommentList)) {
                return;
            }
            List<SociollaProductCommentInfo> sociollaProductCommentInfoList = new ArrayList<>(5);
            userCommentList.forEach(userComment -> {
                SociollaProductCommentInfo sociollaProductCommentInfo = new SociollaProductCommentInfo();
                JSONObject userCommentJson = JSONObject.parseObject(userComment);
                sociollaProductCommentInfo.setComment(userCommentJson.getString("details"));
                List<String> imageUrlList = Lists.newArrayList();
                JSONArray images = userCommentJson.getJSONArray("images");
                for (int index = 0; index < images.size(); index++) {
                    JSONObject image = images.getJSONObject(0);
                    String imageUrl = image.getString("name");
                    imageUrlList.add(imageUrl);
                }
                sociollaProductCommentInfo.setImages(imageUrlList);
                sociollaProductCommentInfo.setCommentId(userCommentJson.getString("_id"));

                Map<String, Object> rantingInfo = new HashMap<>(20);
                rantingInfo.put("long_wear", userCommentJson.getInteger("star_long_wear"));
                rantingInfo.put("pigmentation", userCommentJson.getInteger("star_pigmentation"));
                rantingInfo.put("value_for_money", userCommentJson.getInteger("star_value_for_money"));
                rantingInfo.put("texture", userCommentJson.getInteger("star_texture"));
                rantingInfo.put("packaging", userCommentJson.getInteger("star_packaging"));
                String isRepurchase = userCommentJson.getString("is_repurchase");
                if ("maybe".equalsIgnoreCase(isRepurchase)) {
                    rantingInfo.put("is_repurchase", 1);
                } else if ("yes".equalsIgnoreCase(isRepurchase)) {
                    rantingInfo.put("is_repurchase", 2);
                } else {
                    rantingInfo.put("is_repurchase", 0);
                }
                rantingInfo.put("is_verified_purchase", userCommentJson.getBoolean("is_verified_purchase"));
                sociollaProductCommentInfo.setRantingInfo(rantingInfo);
                sociollaProductCommentInfoList.add(sociollaProductCommentInfo);
            });
            Request request = page.getRequest();
            page.putField("product_id", request.getExtra("product_id"));
            page.putField("current_url", request.getExtra("current_url"));
            page.putField("type", "product_comment");
            page.putField("product_comment", sociollaProductCommentInfoList);
            logger.info("sociollaProductCommentInfoList:{}", JSONObject.toJSONString(sociollaProductCommentInfoList));
        }

    }

    private void buildeProductRating(SociollaProductRatingInfo sociollaProductRatingInfo, String titleNameStar, String categoryStarCountString) {
        titleNameStar = titleNameStar.trim();
        String[] split = categoryStarCountString.split("/");
        if (split == null || split.length <= 0) {
            return;
        }
        Double categoryStarCount = Double.valueOf(split[0]);
        if ("Packaging".equalsIgnoreCase(titleNameStar)) {
            sociollaProductRatingInfo.setPackaging(categoryStarCount);
        } else if ("Pigmentation".equalsIgnoreCase(titleNameStar)) {
            sociollaProductRatingInfo.setPigmentation(categoryStarCount);
        } else if ("Texture".equalsIgnoreCase(titleNameStar)) {
            sociollaProductRatingInfo.setTexture(categoryStarCount);
        } else if ("Value for Money".equalsIgnoreCase(titleNameStar)) {
            sociollaProductRatingInfo.setValueForMoney(categoryStarCount);
        } else if ("Long Wear".equalsIgnoreCase(titleNameStar)) {
            sociollaProductRatingInfo.setLongWear(categoryStarCount);
        }
    }

    private void processProductDetailPage(Page page, String currentUrl) {
        Html html = page.getHtml();
        SociollaProductInfo sociollaProductInfo = new SociollaProductInfo();
        String productInfoStr = RegexUtil.extract(productPattern, html.toString());
        if (StringUtils.isEmpty(productInfoStr)) {
            logger.warn("currentUrl:{} 获取商品信息失败！productInfo:{}", currentUrl, productInfoStr);
            return;
        }
        //设置商品封面图
        String productImageUrl = (String) page.getRequest().getExtra("product_image_url");
        List<String> covers = new ArrayList<>(20);
        if (StringUtils.isNotBlank(productImageUrl)) {
            covers.add(productImageUrl);
        }
        sociollaProductInfo.setCovers(Lists.newArrayList(productImageUrl));

        productInfoStr = productInfoStr + "}";
        JSONObject productInfoJson = null;
        try {
            productInfoJson = JSONObject.parseObject(productInfoStr);
        } catch (Exception e) {
            logger.warn("解析商品信息失败：errMsg:{} productInfoStr:{}", e.getMessage(), productInfoStr);
        }
        if (productInfoJson == null) {
            return;
        }
        String productId = productInfoJson.getString("id");
        String productMasterId = productInfoJson.getString("product_master_id");

        //分类消息
        List<String> categoryList = (List<String>) page.getRequest().getExtra("category_list");
        sociollaProductInfo.setCategory(categoryList);

        sociollaProductInfo.setName(productInfoJson.getString("name"));
        sociollaProductInfo.setDescription(productInfoJson.getString("description"));

        //品牌信息
        Map<String, Object> brandInfo = Maps.newHashMap();
        brandInfo.put("name", html.xpath("//h2[@class='new-style-brand-shade']/text()").get());
        brandInfo.put("ico", html.xpath("//img[@class='magasala img-logo-quickview']/@src").get());
        sociollaProductInfo.setBrandInfo(brandInfo);

        String attributesCombinationsStr = RegexUtil.extract(attributesCombinationsPattern, html.toString());
        if (StringUtils.isBlank(attributesCombinationsStr)) {
            return;
        }
        attributesCombinationsStr = attributesCombinationsStr + "]";
        Map<String, JSONObject> attributesCombinationsMap = new HashMap<>(20);
        JSONArray attributesCombinationArray = JSONArray.parseArray(attributesCombinationsStr);
        for (int index = 0; index < attributesCombinationArray.size(); index++) {
            JSONObject attributesCombination = attributesCombinationArray.getJSONObject(index);
            String attributeId = attributesCombination.getString("id_attribute");
            attributesCombinationsMap.put(attributeId, attributesCombination);
        }
        //获取商品不同规格价格
        String combinations = RegexUtil.extract(combinationsPattern, html.toString());
        if (StringUtils.isEmpty(combinations)) {
            logger.warn("currentUrl:{} 获取商品规格失败！ productInfo:{}", currentUrl, productInfoStr);
            return;
        }
        combinations = combinations + "}";
        JSONObject combinationsJson = JSONObject.parseObject(combinations);
        Set<String> productCombinationsIdSet = combinationsJson.keySet();

        List<String> imageUrlList = Lists.newArrayList();
        List<String> productRatingUrlList = Lists.newArrayList();
        List<Map<String, Object>> specificationsList = Lists.newArrayList();
        productCombinationsIdSet.forEach(productCombinationsId -> {
            Map<String, Object> specifications = Maps.newHashMap();
            specifications.put("combination_id", productCombinationsId);

            JSONObject productCombinationsInfo = combinationsJson.getJSONObject(productCombinationsId);
            JSONArray attributes = productCombinationsInfo.getJSONArray("attributes");
            String shade = null;
            JSONObject attributesValues = productCombinationsInfo.getJSONObject("attributes_values");
            for (int index = 0; index < attributes.size(); index++) {
                Integer attributeId = attributes.getInteger(index);
                JSONObject attributesJsonObject = attributesCombinationsMap.get(String.valueOf(attributeId));
                String group = attributesJsonObject.getString("group");
                specifications.put(group, attributesValues.getString(attributesJsonObject.getString("id_attribute_group")));
                if ("shade".equalsIgnoreCase(group)) {
                    shade = attributesJsonObject.getString("attribute");
                }
                logger.info(" attributesJsonObject:{} shade:{}", attributesJsonObject, shade);
            }
            if (productCombinationsIdSet.size() == 1) {
                List<String> images = html.xpath("//img[@class='img-responsive']/@data-image-small").all();
                images = images.stream().filter(imageUrl -> StringUtils.isNotBlank(imageUrl)).collect(Collectors.toList());
                specifications.put("images", images);
            } else if (productCombinationsIdSet.size() > 1) {
                imageUrlList.add(buildeGetImagesUrl(productId, productCombinationsId));
            }
            if (CollectionUtils.isEmpty(productRatingUrlList)) {
                productRatingUrlList.add(buildeProductRatingUrl(currentUrl, shade, productId));
            }
            specifications.put("orig_price", new BigDecimal(productCombinationsInfo.getBigInteger("price")));
            if (productCombinationsInfo.containsKey("specific_price")) {
                try {
                    JSONObject specificPrice = productCombinationsInfo.getJSONObject("specific_price");
                    BigDecimal reduction = specificPrice.getBigDecimal("reduction");
                    if (reduction.compareTo(BigDecimal.ONE) == -1) {
                        logger.warn("reduction:{}", reduction);
                        //计算折扣后金
                        BigDecimal price = new BigDecimal(productCombinationsInfo.getBigInteger("price"));
                        reduction = price.multiply(reduction);
                    }
                    specifications.put("reduction", reduction);
                } catch (Exception e) {
                    specifications.put("reduction", new BigDecimal(0L));
                }
            }
            specificationsList.add(specifications);
        });
        sociollaProductInfo.setSpecifications(specificationsList);
        page.putField("current_url", currentUrl);
        page.putField("product_id", productId);
        page.putField("type", "product_infno");
        page.putField("product_info", sociollaProductInfo);
        buildeRequest(imageUrlList, 11L).forEach(request -> {
            logger.info("imageUrl:{}", request.getUrl());
            page.addTargetRequest(request);
        });
        buildeRequest(productRatingUrlList, 11L).forEach(request -> {
            page.addTargetRequest(request);
        });
        //暂时不爬取评论
//        buildeProductCommentUrl(productMasterId, productId, currentUrl, 1L).forEach(request -> {
//            page.addTargetRequest(request);
//        });
    }

    private List<Request> buildRequestList(String url, List<String> categoryList, Long priority, Integer minPageNumber, Integer maxPageNumber) {
        List<Request> requestList = new ArrayList<>(20);
        for (int pageNumb = minPageNumber; pageNumb <= maxPageNumber; pageNumb++) {
            Request request = new Request();
            request.setUrl(String.format(url + "?p=%s", pageNumb));
            Map<String, Object> map = new HashMap<>(20);
            map.put("category_list", categoryList);
            request.setExtras(map);
            request.setPriority(priority);
            requestList.add(request);
        }
        return requestList;
    }

    private String buildeGetImagesUrl(String productId, String productAttributeId) {
        return String.format("https://www.sociolla.com/loadImageProductAttribute.php?id_product=%s&id_product_attribute=%s", productId, productAttributeId);
    }

    private String buildeProductRatingUrl(String url, String shade, String productId) {
        if (StringUtils.isBlank(shade)) {
            return String.format("%s?loadStatisticReview=1&review_for=desktop&id_product=%s&ajax=true&_=%s", url, productId, String.valueOf(System.currentTimeMillis()));
        }
        shade = shade.trim().replace(" ", "_");
        return String.format("%s?shade=%s&loadStatisticReview=1&review_for=desktop&id_product=%s&ajax=true&_=%s", url, shade.toLowerCase(), productId, String.valueOf(System.currentTimeMillis()));
    }

    private List<Request> buildeProductCommentUrl(String productMasterId, String productId, String currentUrl, Long priority) {
        List<Request> productCommentUrlList = new ArrayList<>(20);
        for (int pageNumber = 1; pageNumber <= 20; pageNumber++) {
            Request request = new Request();
            request.setUrl(String.format("https://api.soco.id/v2/review/product/%s?page=%s&per_page=5&filter[and0][is_publish]=1,=&sort[created_at]=DESC&custom_data=true", productMasterId, pageNumber));
            Map<String, Object> map = new HashMap<>(20);
            map.put("product_id", productId);
            map.put("current_url", currentUrl);
            request.setExtras(map);
            request.setPriority(priority);
            productCommentUrlList.add(request);
        }
        return productCommentUrlList;
    }

    private void addTargetRequestList(Page page, List<Request> requestList) {
        requestList.forEach(request -> {
            page.addTargetRequest(request);
        });
    }

    private List<Request> buildeRequestList(Map<String, String> productUrlMap, Long priority) {
        List<Request> requestList = new ArrayList<>(20);
        productUrlMap.forEach((productInfoUrl, productImageUrl) -> {
            Request request = new Request();
            request.setUrl(productInfoUrl);
            Map<String, Object> map = new HashMap<>();
            map.put("product_image_url", productImageUrl);
            request.setExtras(map);
            request.setPriority(priority);
            requestList.add(request);
        });
        return requestList;
    }

    private List<Request> buildeRequest(List<String> urlList, Long priority) {
        List<Request> requestList = new ArrayList<>(20);
        urlList.forEach(url -> {
            Request request = new Request();
            request.setUrl(url);
            request.setPriority(priority);
            requestList.add(request);
        });
        return requestList;
    }


    private Map<String, String> getUrlParams(String url) {
        Map<String, String> map = new HashMap<>();
        url = url.replace("?", ";");
        if (!url.contains(";")) {
            return map;
        }
        if (url.split(";").length > 0) {
            String[] arr = url.split(";")[1].split("&");
            for (String s : arr) {
                String key = s.split("=")[0];
                String value = s.split("=")[1];
                map.put(key, value);
            }
            return map;

        } else {
            return map;
        }
    }
}
