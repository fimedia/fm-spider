package com.baocao.business.service.spider.pipeline;

import com.baocao.business.service.DownloadItemService;
import com.baocao.business.service.spider.model.DownloadItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.ArrayList;
import java.util.List;

@Component
public class DownloadItemPipeline implements Pipeline {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DownloadItemService service;

    @Override
    public void process(ResultItems resultItems, Task task) {
        DownloadItem item  = resultItems.get("item");

        List<String> picFiles = new ArrayList<>();
        resultItems.put("files", picFiles);
        service.downloadUrlToFiles(item, ".mp4", true);
    }
}
