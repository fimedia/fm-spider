package com.baocao.business.service.spider;

import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

public abstract class AbstracePageProcessor implements PageProcessor {

    private Site site = Site.me()
            .setTimeOut(60000)
            .setRetryTimes(5)
            .setCycleRetryTimes(5)
            .setSleepTime(1000)
//            .setUserAgent("discover/5.46.2 (iPhone; iOS 9.2; Scale/2.00) Resolution/750*1334 Version/5.46.2 Build/5462005 Device/(Apple Inc.;iPhone7,2) NetType/WiFi")
            .setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36")
            .setDisableCookieManagement(false)
            ;

    @Override
    public Site getSite() {
        return site;
    }


}
