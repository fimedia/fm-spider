package com.baocao.business.service.spider.pipeline;

import com.baocao.business.service.NoteService;
import com.baocao.business.service.spider.model.Note;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.List;

@Component("NoteSavePipeline")
public class NoteSavePipeline implements Pipeline {

    private static final Logger logger = LoggerFactory.getLogger(NoteSavePipeline.class.getName());

    public NoteSavePipeline() {
    }

    @Autowired
    private NoteService noteService;

    @Override
    public void process(ResultItems resultItems, Task task) {
        Note note = resultItems.get("note");
        List<String> picFiles = resultItems.get("pic_files");
        List<String> videoFiles = resultItems.get("video_files");

        noteService.saveNote(note, picFiles, videoFiles);

        logger.info("NoteSavePipeline process finished. for note :{}", note.toString());
    }
}
