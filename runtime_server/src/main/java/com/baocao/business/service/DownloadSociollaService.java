package com.baocao.business.service;

import com.baocao.business.conf.SpiderConf;
import com.baocao.business.service.spider.SociollaSpider;
import com.baocao.business.service.spider.pipeline.DownloadSociollaItemPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.scheduler.PriorityScheduler;

@Service
public class DownloadSociollaService {
    private static Logger logger = LoggerFactory.getLogger(DownloadSociollaService.class);


    @Autowired
    private DownloadSociollaItemPipeline downloadSociollaItemPipeline;

    @Autowired
    private SociollaSpider sociollaSpider;

    @Autowired
    private SpiderConf mConf;

    private Spider getSpider() {
        Spider pinterestSpiderInstance = Spider.create(sociollaSpider).setScheduler(new PriorityScheduler())
                .addPipeline(downloadSociollaItemPipeline).thread(mConf.getSpiderThreadNumber())
                .setExitWhenComplete(false);
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
//                httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy("127.0.0.1", 1087)));
//                httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy(mConf.getProxyUrl(),9180,mConf.getProxyUserName(),mConf.getProxyPassword())));
        pinterestSpiderInstance.setDownloader(httpClientDownloader);
        return pinterestSpiderInstance;
    }

    public void submitJob(String url) {
        Spider spider = getSpider();
        spider.addUrl(url);
        spider.start();
    }
}
