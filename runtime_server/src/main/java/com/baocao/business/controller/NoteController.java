package com.baocao.business.controller;

import com.baocao.business.model.spider.JobTypeEnum;
import com.baocao.business.model.spider.NoteEntity;
import com.baocao.business.model.spider.NoteUserEntity;
import com.baocao.business.protocol.note.FetchResult;
import com.baocao.business.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(value = "/note")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "submitJob")
    public FetchResult submitJob(@RequestParam("job_type") JobTypeEnum jobType,
                                 @RequestParam(name="user_id", required=false)String userId,
                                 @RequestParam(name="note_id", required=false) String noteId,
                                 @RequestParam(name="url", required=false) String url,
                                 @RequestParam(name="force", required=false, defaultValue = "false")boolean force) {
        switch (jobType) {
            case NOTE_FETCH:
                if (!StringUtils.isEmpty(noteId)) {
                    return noteService.fetchNoteByNoteId(noteId, force);
                }
                break;
            case USER_FETCH:
                if (!StringUtils.isEmpty(userId)) {
                    return noteService.fetchUserNotes(userId, force);
                }
                break;
            case URL_FETCH:
            case API_FETCH:
                if (!StringUtils.isEmpty(url)) {
                    return noteService.fetchUserNotesNyUrl(jobType, url, force);
                }
                break;
        }
        FetchResult result = new FetchResult();
        result.setSuccess(false);
        result.setErrorMsg("unknown operation");
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "listNotes")
    public List<NoteEntity> listNotes() {
        return noteService.listNotes();
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "listUsers")
    public List<NoteUserEntity> listUsers() {
        return noteService.listUsers();
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "findNote")
    public List<NoteEntity> findNoteByUserId(@RequestParam("user_id") String userId) {
        return noteService.findNoteByUserId(userId);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "getNote")
    public NoteEntity findNoteByNoteId(@RequestParam("note_id") String noteId) {
        return noteService.findNoteByNoteId(noteId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "download")
    public void zipTools(HttpServletResponse response, HttpServletRequest request) {

        String userId = request.getParameter("user_id");
        String noteId = request.getParameter("note_id");
        String url = request.getParameter("url");
        if (!StringUtils.isEmpty(url)) {
            noteService.downloadNotesByUrl(request, response, url);
        } else if (!StringUtils.isEmpty(userId)) {
            noteService.downloadUser(request, response, userId);
        } else if (!StringUtils.isEmpty(noteId)){
            noteService.downloadNote(request, response, noteId);
        }
    }
}
