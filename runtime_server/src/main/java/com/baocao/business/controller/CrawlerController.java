package com.baocao.business.controller;

import com.baocao.business.model.spider.JobTypeEnum;
import com.baocao.business.protocol.note.FetchResult;
import com.baocao.business.service.DownloadItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/crawler")
public class CrawlerController {


    @Autowired
    private DownloadItemService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "submitJob")
    public FetchResult submitJob(@RequestParam("job_type") JobTypeEnum jobType,
                                 @RequestParam(name="url", required=false) String url,
                                 @RequestParam(name="force", required=false, defaultValue = "false")boolean force) {
        switch (jobType) {
            case URL_FETCH:
                if (!StringUtils.isEmpty(url)) {
                    return service.fetchByUrl(url, force);
                }
                break;
        }
        FetchResult result = new FetchResult();
        result.setSuccess(false);
        result.setErrorMsg("unknown operation");
        return result;
    }

    @RequestMapping(method = RequestMethod.GET, value = "download")
    public void zipTools(HttpServletResponse response, HttpServletRequest request) {

        String userId = request.getParameter("user_id");
        String noteId = request.getParameter("note_id");
        String url = request.getParameter("url");
        if (!StringUtils.isEmpty(url)) {
            service.downloadByUrl(request, response, url);
        }
    }
}
