package com.baocao.business.controller;

import com.baocao.business.protocol.base.BaseJsonRequest;
import com.baocao.business.protocol.base.BaseJsonResponse;
import com.baocao.business.protocol.base.ProtocolConst;
import com.baocao.business.protocol.base.data.StateInfo;
import com.baocao.business.service.RequestParser;
import com.baocao.business.service.RequestParserFactory;
import com.baocao.business.service.ResponseFormatter;
import com.baocao.business.service.ResponseFormatterFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

public class AbstractController {

    @Autowired
    private RequestParserFactory requestParserFactory;

    @Autowired
    private ResponseFormatterFactory responseFormatterFactory;


    protected <ResponseResult> BaseJsonResponse formatResponseResult(ResponseResult result) {
        StateInfo state = new StateInfo();
        state.setCode(ProtocolConst.CODE_SUCCESS);
        state.setExCode(ProtocolConst.EX_CODE_DEFAULT);
        state.setMsg(ProtocolConst.MSG_SUCCESS);

        ResponseFormatter formatter = responseFormatterFactory.getFormatter();
        BaseJsonResponse response = formatter.formatResponse(result, state);
        return response;
    }

    protected BaseJsonResponse getEmptyResponse() {
        StateInfo state = new StateInfo();
        state.setCode(ProtocolConst.CODE_FAILED);
        state.setExCode(ProtocolConst.EX_CODE_DEFAULT);
        state.setMsg(ProtocolConst.MSG_FAILED);

        ResponseFormatter formatter = responseFormatterFactory.getFormatter();
        BaseJsonResponse response = formatter.formatResponse(null, state);
        return response;
    }

    protected <RequestData> RequestData getRequestData(@RequestBody BaseJsonRequest request, Class<RequestData> dataClass) {
        RequestParser parser = requestParserFactory.getParser(request);
        return parser.getData(request, dataClass);
    }
}
