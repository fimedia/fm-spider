package com.baocao.business.controller;

import com.baocao.business.model.spider.FemaleDailyProductEntity;
import com.baocao.business.service.FemaleDailyService;
import com.baocao.server.base.util.ExportUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/female")
public class FemaleDailyController {

    private static final Logger logger = LoggerFactory.getLogger(FemaleDailyController.class);

    @Autowired
    private FemaleDailyService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "submitJob")
    public void submitJob() {
        service.submitJob();
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "list")
    public List<FemaleDailyProductEntity> list() {
        return service.listAll();
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "findProduct")
    public List<FemaleDailyProductEntity> findByBrand(@RequestParam("brand") String brand) {
        return service.findByBrand(brand);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "getTopProduct")
    public List<FemaleDailyProductEntity> getProductsByReview() {
        return service.getProductsByReview();
    }

    //csv download
    @RequestMapping(method = RequestMethod.GET, value = "download")
    public void csvFile(HttpServletResponse response, HttpServletRequest request) {
        String brand = request.getParameter("brand");

        List<Map<String, Object>> dataList = null;

        List<FemaleDailyProductEntity> logList = null;
        if (StringUtils.isEmpty(brand)) {
            logList = service.getProductsByReview();
        }else {
            logList = service.findByBrand(brand);// 查询到要导出的信息
        }

        if (logList.size() == 0) {
            logger.error("无数据导出, brand{}", brand);
            return;
        }
        String columnNames = "id,brand,product,price,rating,review,category1,category2,category3,url";
        String sTitle = columnNames;
        String fName = "brand_";
        String mapKey = columnNames;
        dataList = new ArrayList<>();

        Map<String, Object> map = null;
        for (FemaleDailyProductEntity entity : logList) {
            map = new HashMap<>();

            map.put("id", entity.getId());
            map.put("brand", escapeValue(entity.getBrand()));
            map.put("product", escapeValue(entity.getProduct()));
            map.put("price", escapeValue(entity.getPrice()));
            map.put("rating", escapeValue(entity.getRating()));
            map.put("review", entity.getReview());

            map.put("category1", escapeValue(entity.getCategory1()));
            map.put("category2", escapeValue(entity.getCategory2()));
            map.put("category3", escapeValue(entity.getCategory()));
            map.put("url", escapeValue(entity.getWebPageUrl()));

            dataList.add(map);
        }
        try (final OutputStream os = response.getOutputStream()) {
            ExportUtil.responseSetProperties(fName, response);
            ExportUtil.doExport(dataList, sTitle, mapKey, os);
            logger.info("生成csv文件成功");
            return;
        } catch (Exception e) {
            logger.error("生成csv文件失败", e);
        }
    }

    private String escapeValue(String value) {
        if (value == null) {
            return "";
        }
        if (value.contains(",") || value.contains("\n") || value.contains("\"")) {
            return "\"" + value + "\"";
        }
        return value;
    }
}
