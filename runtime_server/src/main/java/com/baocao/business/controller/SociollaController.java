package com.baocao.business.controller;

import com.baocao.business.service.DownloadSociollaService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sociolla")
public class SociollaController {

    @Autowired
    private DownloadSociollaService downloadSociollaService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "submitJob")
    public void submitJob(@RequestParam(name = "url", required = false, defaultValue = "https://www.sociolla.com/") String url) {
        downloadSociollaService.submitJob(url);
    }



}
