package com.baocao.business;


import com.baocao.business.service.spider.NoteSpider;
import com.baocao.business.service.spider.pipeline.NoteSavePipeline;
import com.baocao.business.service.spider.pipeline.PicSavePipeline;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpiderTest {

    @Autowired
    @Qualifier("NoteSavePipeline")
    private NoteSavePipeline mNoteSavePipeline;

    @Test
    public void testSpiderNoteWithPicture() {
        Spider spider = Spider.create(new NoteSpider())
                .addPipeline(new PicSavePipeline())
                .addPipeline(mNoteSavePipeline)
                .thread(1);
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
        httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy("http-proxy-t1.dobel.cn",9180,"CNHAPPIERFIG34QRO0","6bgCAKru")));
        spider.setDownloader(httpClientDownloader);

        spider.startUrls(Arrays.asList("https://www.xiaohongshu.com/discovery/item/5cb85676000000000f02ed43"));

        spider.start();
    }

    @Test
    public void testSpiderNoteWithVideo() {
        Spider spider = Spider.create(new NoteSpider())
                .addPipeline(new PicSavePipeline())
                .addPipeline(mNoteSavePipeline)
                .thread(1);
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
        httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy("http-proxy-t1.dobel.cn",9180,"CNHAPPIERFIG34QRO0","6bgCAKru")));
        spider.setDownloader(httpClientDownloader);

        spider.startUrls(Arrays.asList("https://www.xiaohongshu.com/discovery/item/5cb03ee6000000000d02d07e"));

        spider.start();
    }
}
