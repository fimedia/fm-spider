package com.baocao.business;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class SeleniumTest {

    @Test
    public void init() {
        String url = "https://femaledaily.com/category/make-up";

        List<String> result = getAllLinks(url);

        for(String str:result) {

        }

    }

    private List<String> getAllLinks(String url) {
        System.setProperty("webdriver.chrome.driver", "/Users/chenle/devtools/chrome/77/chromedriver");// chromedriver服务地址
        WebDriver driver = new ChromeDriver(); // 新建一个WebDriver 的对象，但是new 的是谷歌的驱动
        driver.get(url); // 打开指定的网站
        List<WebElement> elements = driver.findElements(By.xpath("//a"));

        List<String> result = new ArrayList<>();
        for (WebElement webElement : elements) {
            String str = webElement.getAttribute("href");
            result.add(str);
        }
        return result;
    }
}
